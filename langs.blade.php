<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <style>
        th {
            color: #db4b4b;
        }

        .container-fluid{
            margin-top: 50px;
        }
        td {
            width: 150px;
        }
    </style>
</head>
<body>
    <h1 style="color:orange;text-align:center;">Language Comparision(PHP FILE)</h1>

    <div class="container-fluid" style="overflow-y:auto;">
        <div class="row">
            <div class="col">
                <table class="table table-striped" style="table-layout: fixed">


                    <tr>
                        <th style="width: 75px;">Number</th>
                        <th>Key</th>
                        <th>English</th>
{{--                        <th>Bahasa key</th>--}}
                        <th>Bahasa</th>
{{--                        <th>Chinese HK key</th>--}}
                        <th>Chinese HK</th>
{{--                        <th>Chinese TW key</th>--}}
                        <th>Chinese TW</th>
                    </tr>
                    @foreach($builtEnglishLangArrayKeys as $key => $builtEnglishLangArrayKey)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{ $builtEnglishLangArrayKey }}</td>
                            <td>{{ $builtEnglishLangArrayValues[$key] }}</td>
{{--                            <td>{{$orderedBahasaLangArrayKeys[$key]}}</td>--}}
                            @if($builtEnglishLangArrayValues[$key]==$orderedBahasaLangArrayValues[$key])
                                <td style="color: red;">{{$orderedBahasaLangArrayValues[$key]}}</td>
                            @else
                                <td>{{$orderedBahasaLangArrayValues[$key]}}</td>
                            @endif
{{--                            <td>{{$orderedChineseHKLangArrayKeys[$key]}}</td>--}}
                            @if($builtEnglishLangArrayValues[$key]==$orderedChineseHKLangArrayValues[$key])
                                <td style="color: red;">{{$orderedChineseHKLangArrayValues[$key]}}</td>
                            @else
                                <td>{{$orderedChineseHKLangArrayValues[$key]}}</td>
                            @endif
{{--                            <td>{{$orderedChineseTWLangArrayKeys[$key]}}</td>--}}
                            @if($builtEnglishLangArrayValues[$key]==$orderedChineseTWLangArrayValues[$key])
                                <td style="color: red;">{{$orderedChineseTWLangArrayValues[$key]}}</td>
                            @else
                                <td>{{$orderedChineseTWLangArrayValues[$key]}}</td>
                            @endif
                        </tr>
                    @endforeach
                </table>

            </div>

        </div>
        </div>

    </div>
</body>
</html>