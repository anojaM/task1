<?php

namespace Zuzu\Http\Controllers\Frontend;

use Carbon\Carbon;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Exception;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Zuzu\Http\Controllers\Controller;
use Zuzu\Jobs\RoomType\AddRoomTypeNameHotelJob;
use Zuzu\Libs\Booking\BookingProcess;
use Zuzu\Libs\Channel\ChannelConnect;
use Zuzu\Libs\Channel\Staah\StaahConnect;
use Zuzu\Libs\Invoice\BookingInvoiceBarData;
use Zuzu\Libs\Invoice\BookingInvoiceData;
use Zuzu\Libs\Log\AppLogger;
use Zuzu\Libs\Payment\PaymentData;
use Zuzu\Libs\RatePlan\RatePlan;
use Zuzu\Libs\ZuzuLibs\ZuzuLibs;
use Zuzu\Models\Booking\BookingModel;
use Zuzu\Models\Booking\BookingStayDatesModel;
use Zuzu\Models\Booking\BookingUpdatesModel;
use Zuzu\Models\Channel\ChannelBookingModel;
use Zuzu\Models\Channel\ChannelBookingXMLModel;
use Zuzu\Models\Channel\ChannelCountryOtaModel;
use Zuzu\Models\Channel\ChannelMapModel;
use Zuzu\Models\Channel\ChannelModel;
use Zuzu\Models\Channel\HotelChannelModel;
use Zuzu\Models\Channel\OTAChannelModel;
use Zuzu\Models\Channel\OTAChannelPaymentMethodMapModel;
use Zuzu\Models\Currency\CurrencyModel;
use Zuzu\Models\Hotel\HotelModel;
use Zuzu\Models\Hotel\HotelOTAChannelModel;
use Zuzu\Models\Hotel\HotelPaymentMethodsModel;
use Zuzu\Models\Hotel\HotelUserModel;
use Zuzu\Models\Neighbour\NeighbourContentModel;
use Zuzu\Models\Neighbour\NeighbourModel;
use Zuzu\Models\Payment\Booking2C2PModel;
use Zuzu\Models\Payment\BookingPaymentModel;
use Zuzu\Models\Payment\SetHotelPaymentModel;
use Zuzu\Models\RatePlan\HotelRatePlanContentModel;
use Zuzu\Models\RatePlan\HotelRatePlanDurationModel;
use Zuzu\Models\RatePlan\HotelRatePlanModel;
use Zuzu\Models\RatePlan\HotelRatePlanRoomsModel;
use Zuzu\Models\RatePlan\HotelRatesRawModel;
use Zuzu\Models\RatePlan\RatePlanModel;
use Zuzu\Models\Reports\InvoiceModel;
use Zuzu\Models\RoomType\HotelRoomTypeDetailedAvailabilityModel;
use Zuzu\Models\RoomType\RoomTypeModel;
use Zuzu\Models\RoomType\RoomTypeRateModel;
use Zuzu\Models\User\ActivationModel;
use Zuzu\Models\User\UserModel;
use Zuzu\Repositories\Booking\BookingRepositoryInterface;
use Zuzu\Repositories\Channel\ChannelRepositoryInterface;
use Zuzu\Repositories\City\CityRepositoryInterface;
use Zuzu\Repositories\Hotel\HotelOTAChannelRepositoryInterface;
use Zuzu\Repositories\Hotel\HotelRepositoryInterface;
use Zuzu\Repositories\Price\PriceRepositoryInterface;
use Zuzu\Repositories\User\UserRepositoryInterface;
use Zuzu\Repositories\RoomType\RoomTypeRepositoryInterface;

class ToDoController extends Controller
{
    use DispatchesJobs;

    private $bookingRepo;
    private $hotelRepo;
    private $priceRepo;
    private $successLogger;
    private $errorsLogger;
    private $cityRepo;
    private $hotelOTAChannelRepo;
    private $channelRepo;
    private $userRepo;
    private $roomTypeRepo;
    private $hotelRatesRawModel;

    /**
     * Constructor.
     * @param BookingRepositoryInterface $bookingRepo
     * @param HotelRepositoryInterface $hotelRepo
     * @param PriceRepositoryInterface $priceRepo
     * @param CityRepositoryInterface $cityRepo
     * @param HotelOTAChannelRepositoryInterface $hotelOTAChannelRepo
     * @param ChannelRepositoryInterface $channelRepo
     * @param UserRepositoryInterface $userRepo
     * @param RoomTypeRepositoryInterface $roomTypeRepo
     * @param HotelRatesRawModel $hotelRatesRawModel
     */
    public function __construct(
        BookingRepositoryInterface $bookingRepo,
        HotelRepositoryInterface $hotelRepo,
        PriceRepositoryInterface $priceRepo,
        CityRepositoryInterface $cityRepo,
        HotelOTAChannelRepositoryInterface $hotelOTAChannelRepo,
        ChannelRepositoryInterface $channelRepo,
        UserRepositoryInterface $userRepo,
        RoomTypeRepositoryInterface $roomTypeRepo,
        HotelRatesRawModel $hotelRatesRawModel
    )
    {
        $this->bookingRepo = $bookingRepo;
        $this->hotelRepo = $hotelRepo;
        $this->priceRepo = $priceRepo;
        $this->successLogger = AppLogger::getBookingSuccessLogger();
        $this->errorsLogger = AppLogger::getBookingErrorLogger();
        $this->cityRepo = $cityRepo;
        $this->hotelOTAChannelRepo = $hotelOTAChannelRepo;
        $this->channelRepo = $channelRepo;
        $this->userRepo = $userRepo;
        $this->roomTypeRepo = $roomTypeRepo;
        $this->hotelRatesRawModel = $hotelRatesRawModel;
    }

    public function getUpdateUserRegistrationNumbers()
    {
//        $user = UserModel::find(83);
//        dd(Activation::completed($user));

        $allUsers = UserModel::where('status', '!=', NOT_REGISTERED_BOOKING_USER_STATUS)->get();

        $frontendUsers = [];
        $backendUsers = [];
        foreach ($allUsers as $user) {
            if ($user->inRole(ROLE_FRONTEND_USER) || $user->inRole(ROLE_FRONTEND_USER_CHILD)) {
                $frontendUsers[] = $user;
            } else {
                $backendUsers[] = $user;
            }
        }

        $backendUpdated = [];
        foreach ($backendUsers as $user) {
            $changed = false;

            if (!$user->loyalty_registered_date && $user->registration_number && $user->created_at) {
                $user->loyalty_registered_date = $user->created_at;
                $changed = true;
            }

            if (!$user->registration_number) {
                $n = $this->generateRegistrationNumber($user);

                if ($n !== false) {
                    $user->registration_number = $n;
                    $changed = true;
                }
            }

            if (!$user->slug) {
                $user->slug = UserModel::createSlug($user->first_name . " " . $user->last_name);
                $changed = true;
            }

            if ($changed) {
                $user->save();
                $backendUpdated[] = $user->id;
            }
        }

        $frontendUpdated = [];
        foreach ($frontendUsers as $user) {
            $changed = false;

            if (Activation::completed($user)) {
                if (!$user->loyalty_registered_date && $user->registration_number && $user->created_at) {
                    $user->loyalty_registered_date = $user->created_at;
                    $changed = true;
                }

                if (!$user->registration_number) {
                    $n = $this->generateRegistrationNumber($user);

                    if ($n !== false) {
                        $user->registration_number = $n;
                        $changed = true;
                    }
                }
            }

            if (!$user->slug) {
                $user->slug = UserModel::createSlug($user->first_name . " " . $user->last_name);
                $changed = true;
            }

            if ($changed) {
                $user->save();
                $frontendUpdated[] = $user->id;
            }
        }

        dd($backendUpdated, $frontendUpdated);
    }

    private $a = [];
    private $c = 2;

    public function getExportMissingLanguages()
    {
        $files = $files = File::files(base_path('resources/lang/en'));
        $languages = [
            'id' => 'Indonesian',
            'zh-HK' => 'HK-Chinese',
            'zh-TW' => 'TW-Chinese'
        ];

        foreach ($languages as $key => $lang) {
            Excel::create('ZUZU_lang_' . strtoupper($key), function ($excel) use ($files, $key, $lang) {
                foreach ($files as $file) {
                    $this->a = [];
                    $this->c = 2;
                    $fileName = pathinfo($file, PATHINFO_FILENAME);
                    App::setLocale('en');
                    $en = trans($fileName);

                    if (File::exists(base_path('resources/lang/' . $key . '/' . $fileName . '.php'))) {
                        App::setLocale($key);
                        $id = trans($fileName);
                    } else {
                        $id = [];
                    }
                    App::setLocale('en');

                    $arrDiff = $this->arrayRecursiveDiff($en, $id);

                    if (count($arrDiff) == 0) {
                        continue;
                    }

                    $excel->sheet($fileName, function ($sheet) use ($arrDiff, $lang) {

                        $this->rec($sheet, $arrDiff, 0);
                        $sheet->row(1, ['KEY', "ENGLISH", strtoupper($lang)]);
                        $sheet->row(1, function ($row) {
                            $row->setBackground('#F36F31')->setFont(array(
                                'size' => '16',
                                'bold' => true
                            ));
                        });
                        $sheet->freezeFirstRowAndColumn();
                    });
                }
            })->store('xlsx', storage_path('excel/exports'));
        }
    }

    private function rec($sheet, $data, $k = null)
    {
        if (is_array($data)) {
            foreach ($data as $k => $v) {
                $this->a[] = $k;
                if (is_array($data)) {
                    $this->rec($sheet, $v, $k, $this->a);
                }
            }

            array_pop($this->a);
        } else {
            $sheet->row($this->c, [implode('.', $this->a), $data]);
            $this->c++;
            array_pop($this->a);
        }
    }

    private function arrayRecursiveDiff($aArray1, $aArray2)
    {
        $aReturn = array();

        foreach ($aArray1 as $mKey => $mValue) {
            if (array_key_exists($mKey, $aArray2)) {
                if (is_array($mValue)) {
                    $aRecursiveDiff = $this->arrayRecursiveDiff($mValue, $aArray2[$mKey]);
                    if (count($aRecursiveDiff)) {
                        $aReturn[$mKey] = $aRecursiveDiff;
                    }
                }

//                Check if same value as in English also in other language file
                if ($mValue === $aArray2[$mKey]) {
                    $aReturn[$mKey] = $mValue;
                }
            } else {
                $aReturn[$mKey] = $mValue;
            }
        }
        return $aReturn;
    }

    private function generateRegistrationNumber($user)
    {

        $date = $user->loyalty_registered_date ?
            Carbon::parse($user->loyalty_registered_date) :
            Carbon::parse($user->created_at);

        do {
            $letters = generateRandomString(2);
            $numbers = rand(1000, 9999);
            $d = $date;
            $userP = $d->format('m') . $d->format('y');

            $n = $userP . $letters . $numbers;

            $exists = UserModel::where('registration_number', $n)->first() ? true : false;
        } while ($exists);

        return $n;
    }

    public function getChangeHotelSlug()
    {
        set_time_limit(0);

        $allHotels = HotelModel::select('Id', 'trn_Hotel.Slug', 'DisplayName')
            ->join('trn_HotelContent', 'trn_HotelContent.HotelId', '=', 'trn_Hotel.Id')
            ->where('languageId', DEFAULT_LANGUAGE_ID)
            ->get()
            ->chunk(500);

        foreach ($allHotels as $hotel) {
            $hotel->each(function ($v) {
                $v->newSlug = HotelModel::createSlug($v->DisplayName);
                $v->save();
            });
        }


        dd("Done");
    }

    public function getSlug($name)
    {
        $slug = HotelModel::createSlug($name);

        $c = 0;
        do {
            if (isset($exists)) {
                $slug .= "-" . $c;
            }
            $exists = HotelModel::where('newSlug', $slug)->first() ? true : false;
            $c++;
        } while ($exists);

        return $slug;
    }

    public function getGenerateHotelSlugRedirection()
    {
        set_time_limit(0);

        try {
            $allHotels = HotelModel::select('Id', 'trn_Hotel.Slug', 'newSlug', 'DisplayName')
                ->join('trn_HotelContent', 'trn_HotelContent.HotelId', '=', 'trn_Hotel.Id')
                ->where('languageId', DEFAULT_LANGUAGE_ID)
                ->get()
                ->chunk(500);

            $missed = [];
            $path = public_path() . '/redirection_old.txt';

            $urls = ['hotel_view' => "/hotel/view/", 'checkout' => "/checkout/"];
            foreach ($urls as $key => $url) {
                $name = strtoupper(implode(" ", explode('_', $key)));
                File::append($path, "\n\n\n" . "## OLD HOTEL SLUG REDIRECTION IN {$name} PAGE ##");
                $base = $url;
                foreach ($allHotels as $hotel) {
                    $hotel->each(function ($v) use ($path, $base, &$missed) {
                        $rFrom = $v->Slug;
                        $rTo = $v->newSlug;

                        if ($rFrom && $rTo && trim($rFrom) != "" && trim($rTo) != "") {
                            File::append($path, "\n" . "Redirect {$base}" . $rFrom . " " . "{$base}" . $rTo);
                        } else {
                            $missed[] = $v->Id;
                        }
                    });
                }
            }

            dd("done", $missed);
        } catch (Exception $e) {
            dd($e);
        }
    }

    /**
     * Generate inactive user report
     */
    public function getGenerateUserReport()
    {
        $allUsers = UserModel::all();

        $frontendUsers = [];
        foreach ($allUsers as $user) {
            if ($user->inRole(ROLE_FRONTEND_USER) || $user->inRole(ROLE_FRONTEND_USER_CHILD)) {
                $frontendUsers[] = $user;
            }
        }

        $frontendInActiveUsers = [];
        foreach ($frontendUsers as $user) {
            if ($user->status != USER_ACTIVE_STATUS) {
                $frontendInActiveUsers[] = $user;
            }
        }

        $frontendHasBookingInactiveUsers = [];
        $frontendNoBookingUsers = [];

        foreach ($frontendInActiveUsers as $user) {
            $hasBooking = DB::table('trn_Booking')->where('BookingUserID', $user->id)->count() > 0;

            $status = "";

            if ($user->status == USER_ACTIVE_STATUS) {
                $status = "Active";
            } elseif ($user->status == USER_INACTIVE_STATUS) {
                $status = "Inactive";
            } elseif ($user->status == USER_DELETED_STATUS) {
                $status = "Deleted";
            } elseif ($user->status == NOT_REGISTERED_BOOKING_USER_STATUS) {
                $status = "Not Registered Booking User";
            }

            $uArray = [
                $user->id,
                $user->email,
                $user->first_name . " " . $user->last_name,
                $status,
                $user->created_at,
                $user->updated_at
            ];

            if ($hasBooking) {
                $frontendHasBookingInactiveUsers[] = $uArray;
            } else {
                $frontendNoBookingUsers[] = $uArray;
            }
        }

        Excel::create('ZUZU_INACTIVE_FRONTEND_USER_REPORT_' . date('Y_m_d'), function ($excel) use (
            $frontendHasBookingInactiveUsers,
            $frontendNoBookingUsers
        ) {
            $users = ["has_booking" => $frontendHasBookingInactiveUsers, 'no_bookings' => $frontendNoBookingUsers];

            foreach ($users as $k => $userPartials) {
                $excel->sheet($k, function ($sheet) use ($userPartials) {
                    $sheet->row(1, ['ID', "EMAIL", 'NAME', 'STATUS', 'CREATED_DATE', 'UPDATED_DATE']);
                    $sheet->row(1, function ($row) {
                        $row->setBackground('#F36F31')->setFont(array(
                            'size' => '16',
                            'bold' => true
                        ));
                    });
                    $sheet->freezeFirstRowAndColumn();

                    $c = 1;
                    foreach ($userPartials as $user) {
                        $c++;
                        $sheet->row($c, $user);
                    }
                });
            }
        })->store('xls', storage_path('excel/exports'));

        dd($frontendHasBookingInactiveUsers, $frontendNoBookingUsers);
    }

    public function getAddLocationSlug()
    {
        $all = LocationModel::all();

        foreach ($all as $l) {
            $content = LocationContentModel::select('LocationName')
                ->where('PreDefinedLocationID', $l->Id)
                ->where('LanguageID', DEFAULT_LANGUAGE_ID)
                ->first();

            $slug = LocationModel::createSlug($content->LocationName);

            $l->Slug = $slug;
            $l->Save();
        }

        dd("DONE");
    }

    public function getAddNeighboursSlug()
    {
        $all = NeighbourModel::all();

        foreach ($all as $l) {
            $content = NeighbourContentModel::select('NeighbourName')
                ->where('PredefinedNeighbourID', $l->Id)
                ->where('LanguageID', DEFAULT_LANGUAGE_ID)
                ->first();

            $slug = NeighbourModel::createSlug($content->NeighbourName);

            $l->Slug = $slug;
            $l->Save();
        }

        dd("DONE");
    }


    public function getRemoveMemberShipNumberFromAdmin()
    {
        $allUsers = UserModel::all();

        $report = [];
        foreach ($allUsers as $user) {
            if (!($user->inRole(ROLE_FRONTEND_USER) || $user->inRole(ROLE_FRONTEND_USER_CHILD))) {
                $report[] = [
                    'id' => $user->id,
                    'registration_number' => $user->registration_number,
                    'loyalty_registered_date' => $user->loyalty_registered_date
                ];

                $user->registration_number = null;
                $user->loyalty_registered_date = null;
                $user->save();
            }
        }

        Mail::send([], [], function ($message) use ($report) {
            $message->to('kalhan.p@eyepax.com')/*->cc('derick.k@eyepax.com')*/
            ->subject('Admin users: remove registration and loyalty dates')
                ->subject('Admin users: remove registration and loyalty dates')
                ->setBody(json_encode($report));
        });

        dd("DONE");
    }

    public function getExportAccountHolders()
    {
        $allUsers = UserModel::all();

        $accountVerified = [];
        $accountUnVerified = [];
        $unregisteredUsers = [];
        foreach ($allUsers as $user) {
            if ($user->inRole(ROLE_FRONTEND_USER) || $user->inRole(ROLE_FRONTEND_USER_CHILD)) {
                if ($user->status != USER_DELETED_STATUS) {
                    if (Activation::completed($user)) {
                        $accountVerified[] = $user;
                    } else {
                        $activationModel = ActivationModel::where('user_id', $user->id)->where('completed', 0)->first();
                        if ($activationModel) {
                            $accountUnVerified[] = $user;
                        } else {
                            $unregisteredUsers[] = $user;
                        }
                    }
                }
            }
        }

        $users = [
            'Account verified' => $accountVerified,
            'Account  unverified' => $accountUnVerified,
            'Unregistered users' => $unregisteredUsers
        ];

        Excel::create('ZUZUROOMS_ACCOUNT_HOLDERS_REPORT_' . date('Y_m_d'), function ($excel) use (
            $users
        ) {
            foreach ($users as $i => $user_partial) {
                $excel->sheet($i, function ($sheet) use ($user_partial) {
                    $sheet->row(1, ['FIRST NAME', "LAST NAME", 'DIAL_CODE', 'PHONE', 'STATUS']);
                    $sheet->row(1, function ($row) {
                        $row->setBackground('#F36F31')->setFont(array(
                            'size' => '16',
                            'bold' => true
                        ));
                    });
                    $sheet->freezeFirstRowAndColumn();

                    foreach ($user_partial as $i => $user) {
                        $status = "active";
                        if ($user->status == USER_INACTIVE_STATUS) {
                            $status = "inactive";
                        } elseif ($user->status == USER_DELETED_STATUS) {
                            $status = "deleted";
                        } elseif ($user->status == NOT_REGISTERED_BOOKING_USER_STATUS) {
                            $status = "not registered booking user";
                        }
                        $sheet->row($i + 2, [
                            $user->first_name,
                            $user->last_name,
                            $user->telephone_no_dial_code ?: $user->mobile_no_dial_code,
                            $user->telephone_no ?: $user->mobile_no,
                            $status
                        ]);
                    }
                });
            }
        })->store('xls', storage_path('excel/exports'));

        dd($users);
    }

    public function getRegisterEmailFUsers()
    {
        $allUsers = UserModel::all();
        set_time_limit(0);

        $mailUsers = [];
        foreach ($allUsers as $user) {
            if ($user->inRole(ROLE_FRONTEND_USER) || $user->inRole(ROLE_FRONTEND_USER_CHILD)) {
                if ($user->status != USER_DELETED_STATUS) {
                    if (!Activation::completed($user) && $user->status != USER_DELETED_STATUS) {
//                        dd(Activation::exists($user), $user->id);
                        $mailUsers[] = $user;
                    }
                }
            }
        }

        $userx = [];
        foreach (array_slice($mailUsers, 27) as $user) {
            $activation = Activation::exists($user);
            if (!$activation) {
                ActivationModel::where('user_id', $user->id)->where('completed', 0)->delete();
                $activation = Activation::create($user);
            }

            $user->activation = $activation;
            $userx[] = $user;

            Mail::queue([], [], function ($message) use ($user) {
                $user->email = 'kalhan.p@eyepax.com';

                $message->to($user->email)
                    ->subject('Admin users: remove registration and loyalty dates')
                    ->subject('Admin users: remove registration and loyalty dates')
                    ->setBody(json_encode($user));
            });
        }

//        dd($userx);

        return response()->json($userx);
    }

    public function getUpdateHotelBookingAmount()
    {
        try {
            $bookings = BookingModel::select(
                'trn_Booking.ID as id',
                'trn_Booking.BookingTotalAmount as total_amount',
                'trn_Booking.BookingHotelTaxAmount as tax_amount',
                'trn_Booking.BookingHotelCommissionAmount as commission_amount',
                'sys_Currency.Id as currency_id',
                'sys_Currency.CurrencyCode as currency_code',
                'sys_Currency.CurrencySymbol as currency_symbol',
                'sys_Currency.CurrencyRate as currency_rate',
                'sys_CurrencyContent.CurrencyName as currency_name'
            )
                ->join('trn_Hotel', 'trn_Hotel.Id', '=', 'trn_Booking.HotelID')
                ->join('sys_Currency', 'sys_Currency.Id', '=', 'trn_Hotel.CurrencyID')
                ->join('sys_CurrencyContent', 'sys_CurrencyContent.CurrencyID', '=', 'sys_Currency.Id')
                ->whereNotNull('trn_Hotel.CurrencyID')
                ->where(function ($q) {
                    $q->whereNull('trn_Booking.HotelTotalAmount');
                    $q->orWhere('trn_Booking.HotelTotalAmount', 0);
                })
                ->where('sys_CurrencyContent.LanguageID', DEFAULT_LANGUAGE_ID)
                ->take(10)->get();

            foreach ($bookings as $key => $booking) {
                BookingModel::where('ID', $booking->id)->update([
                    'HotelTotalAmount' => $booking->total_amount * $booking->currency_rate,
                    'HotelTaxAmount' => $booking->tax_amount * $booking->currency_rate,
                    'HotelCommissionAmount' => $booking->commission_amount * $booking->currency_rate,
                    'HotelCurrencyID' => $booking->currency_id,
                    'HotelCurrencyName' => $booking->currency_name,
                    'HotelCurrencyCode' => $booking->currency_code,
                    'HotelCurrencyRate' => $booking->currency_rate
                ]);
            }

            return $bookings;
        } catch (\Exception $e) {
            dd($e);
        }
    }

    public function getExportRewardsMembers()
    {
        $allUsers = UserModel::whereNotNull('loyalty_registered_date')->get();

        $users = [];
        foreach ($allUsers as $user) {
            if ($user->inRole(ROLE_FRONTEND_USER) || $user->inRole(ROLE_FRONTEND_USER_CHILD)) {
                $users[] = $user;
            }
        }

        Excel::create('ZUZUREWARDS_ACCOUNT_HOLDERS_REPORT_' . date('Y_m_d'), function ($excel) use ($users) {
            $excel->sheet('1', function ($sheet) use ($users) {
                $sheet->row(1, ['STATUS', "EMAIL", "FIRST_NAME", "LAST_NAME", 'COUNTRY', 'MEMBERSHIP_NUMBER']);
                $sheet->row(1, function ($row) {
                    $row->setBackground('#F36F31')->setFont(array(
                        'size' => '16',
                        'bold' => true
                    ));
                });

                $sheet->freezeFirstRowAndColumn();

                foreach ($users as $i => $user) {
                    $status = "active";
                    if ($user->status == USER_INACTIVE_STATUS) {
                        $status = "inactive";
                    } elseif ($user->status == USER_DELETED_STATUS) {
                        $status = "deleted";
                    } elseif ($user->status == NOT_REGISTERED_BOOKING_USER_STATUS) {
                        $status = "not registered booking user";
                    }
                    $sheet->row($i + 2, [
                        $status,
                        $user->email,
                        $user->first_name,
                        $user->last_name,
                        $user->country_id ?: $user->telephone_no_dial_code,
                        $user->registration_number
                    ]);
                }
            });
        })->store('xls', storage_path('excel/exports'));

        dd($users);
    }

    public function getExportNonRewardsMembers()
    {
        $allUsers = UserModel::whereNull('loyalty_registered_date')->get();

        $users = [];
        foreach ($allUsers as $user) {
            if ($user->inRole(ROLE_FRONTEND_USER) || $user->inRole(ROLE_FRONTEND_USER_CHILD)) {
                $users[] = $user;
            }
        }

        $countries = DB::table('set_Country')->lists('Code', 'CountryTelephoneCode');

        Excel::create('NON_ZUZUREWARDS_ACCOUNT_HOLDERS_REPORT_' . date('Y_m_d'), function ($excel) use (
            $users,
            $countries
        ) {
            $excel->sheet('1', function ($sheet) use ($users, $countries) {
                $sheet->row(1, ['STATUS', "EMAIL", "FIRST_NAME", "LAST_NAME", 'COUNTRY']);
                $sheet->row(1, function ($row) {
                    $row->setBackground('#F36F31')->setFont(array(
                        'size' => '16',
                        'bold' => true
                    ));
                });

                $sheet->freezeFirstRowAndColumn();

                foreach ($users as $i => $user) {
                    $status = "active";
                    if ($user->status == USER_INACTIVE_STATUS) {
                        $status = "inactive";
                    } elseif ($user->status == USER_DELETED_STATUS) {
                        $status = "deleted";
                    } elseif ($user->status == NOT_REGISTERED_BOOKING_USER_STATUS) {
                        $status = "not registered booking user (inactive)";
                    }

                    if ($user->country_id) {
                        $countryC = $user->country_id;
                    } elseif ($user->telephone_no_dial_code != "+00") {
                        $countryC = $countries[$user->telephone_no_dial_code];
                    } else {
                        if ($user->mobile_no[0] != "+") {
                            $user->mobile_no = '+' . $user->mobile_no;
                        }

                        foreach ($countries as $key => $country) {
                            if (strpos($user->mobile_no, $key) !== false &&
                                strpos($user->mobile_no, $key) == 0 && $key != "+") {
                                $user->mobile_no = $country;
                            }
                        }

                        $countryC = $user->mobile_no;
                    }

                    $sheet->row($i + 2, [
                        $status,
                        $user->email,
                        $user->first_name,
                        $user->last_name,
                        $countryC
                    ]);
                }
            });
        })->store('xls', storage_path('excel/exports'));

        dd($users);
    }

    public function getExportEmailSignUps()
    {
        $users = DB::table('trn_SubscribedUsers')->get();

        Excel::create('EMAIL_SIGNUP_USER_REPORT_' . date('Y_m_d'), function ($excel) use ($users) {
            $excel->sheet('1', function ($sheet) use ($users) {
                $sheet->row(1, ["EMAIL", 'COUNTRY', 'IP']);
                $sheet->row(1, function ($row) {
                    $row->setBackground('#F36F31')->setFont(array(
                        'size' => '16',
                        'bold' => true
                    ));
                });

                $sheet->freezeFirstRowAndColumn();

                foreach ($users as $i => $user) {
                    $sheet->row($i + 2, [
                        $user->Email,
                        $user->Country,
                        $user->IpAddress
                    ]);
                }
            });
        })->store('xls', storage_path('excel/exports'));

        dd($users);
    }

    public function getNonCurrencyUsers()
    {
        try {
            $query = UserModel::select(
                'trn_Users.id AS ID',
                'trn_Users.email AS EMAIL',
                'trn_Users.first_name AS NAME',
                'trn_Users.status AS STATUS',
                'trn_Users.country_id AS COUNTRY CODE',
//                'set_CountryContent.Country AS COUNTRY',
                'trn_Users.created_at AS CREATED_DATE',
                'trn_Users.updated_at AS UPDATED_DATE'
            )
//                ->leftJoin('set_CountryContent', 'set_CountryContent.Code', '=', 'trn_Users.country_id')
                ->whereNotNull('trn_Users.loyalty_registered_date')
                ->where(function ($q) {
                    $q->where('trn_Users.currency_id', 0);
                    $q->orWhereNull('trn_Users.currency_id');
                })
//                ->where('set_CountryContent.LanguageID', DEFAULT_LANGUAGE_ID);
                ->get();

            Excel::create('ZUZU_NON_CURRENCY_USERS_' . date('Y_m_d'), function ($excel) use ($query) {
                $excel->sheet('test', function ($sheet) use ($query) {
                    $sheet->row(1, ['ID', "EMAIL", 'NAME', 'STATUS', 'CREATED_DATE', 'UPDATED_DATE']);
                    $sheet->row(1, function ($row) {
                        $row->setBackground('#F36F31')->setFont(array(
                            'size' => '16',
                            'bold' => true
                        ));
                    });
                    $sheet->freezeFirstRowAndColumn();
                    $sheet->fromArray($query);
                });
            })->store('xls', storage_path('excel/exports'));

            return $query;
        } catch (Exception $e) {
            dd();
        }
    }

    public function getCreateBooking()
    {
        dd('test');

        $data = [
            "checkin" => "2016-11-05",
            "checkout" => "2016-11-06",
            "guests" => 2,
            "rooms" => 1,
            "first_name" => "Eny Purnawaty",
            "last_name" => "Purnawaty",
            "email" => "xfdr.r@eyepax.com", // l1pss@yahoo.com
            "mobile" => "85710186894",
            "address" => null,
            "hotel" => "xxyy", // zuzu-seibu-grand-indonesia
            "coupon_code" => "", // ZUZU49K
            "room_type" => "3", // 1
            "bookingId" => null,
            "reward_night" => null,
            "payment_type" => 1,
            "dial_code" => "+62",
            "password" => "",
            "current_rate" => 9352.75,
            "currencyId" => 2, // 233
            "language" => 1
        ];

        $hotel = $this->hotelRepo->getHotelBySlug($data['hotel'], DEFAULT_LANGUAGE_ID);

        $priceModelParams = [
            'code' => $hotel->country_code,
            'currency' => $data['currencyId'],
            'city' => $hotel->city,
            'hotel_type' => $hotel->category
        ];

        $checkIn = Carbon::parse($data['checkin']);
        $checkOut = Carbon::parse($data['checkout']);
        $data['days'] = $checkIn->diffInDays($checkOut);

        $priceModelValues = $this->priceRepo
            ->getMaxPriceDetailsForLocationCurrency($priceModelParams, $checkIn, $checkOut);

        $data['price_model_total'] = $priceModelValues[0]->amount * $data['rooms'] * $data['days'];
        $data['price_model_tax'] = getTax($data['price_model_total'], $hotel->tax);
        $data['price_model_currency_id'] = $priceModelValues[0]->currency_id;

        $save = $this->bookingRepo->saveBooking($data, null);

        if ($save['success']) {
            // Create QR code
            QrCode::format(QR_CODE_FORMAT)
                ->size(QR_CODE_SIZE)
                ->margin(QR_CODE_MARGIN)
                ->generate(
                    urlWithSchema('booking/confirmation/' . base64_encode($save['bookingId'])),
                    QR_CODE_STORE_PATH . base64_encode($save['bookingId']) . '.' . QR_CODE_FORMAT
                );
            dd('booking success');

            // If booking get success change payment status
        } else {
            dd('booking failed');
        }
    }

    public function getSendBookingConfirmationEmail()
    {
        $common = App::make('Zuzu\Listeners\Common\SendBookingEmail');
        $confirm = App::make('Zuzu\Listeners\ChannelBookingConfirmationEventListener');
        $cancel = App::make('Zuzu\Listeners\Channel\ChannelBookingStatusChangeEventListener');

        $channelBookings = ChannelBookingModel::select(
            'trn_ChannelBooking.ChannelBookingId',
            'trn_ChannelBooking.BookingId',
            'trn_Booking.BookingStatus'
        )
            ->join(
                'trn_Booking',
                'trn_Booking.ID',
                '=',
                'trn_ChannelBooking.BookingId'
            )
            ->whereIn('ChannelBookingId', [
            ]);

        dd('---');

        $uniqueChannelBookings = $channelBookings->groupBy('ChannelBookingId')->get();
        $allChannelBookings = $channelBookings->get();
        $cancelEmail = [];
        $confirmEmail = [];
        $noEmail = [];

        foreach ($uniqueChannelBookings as $channelBooking) {
            $bookingIds = $allChannelBookings
                ->where('ChannelBookingId', $channelBooking->ChannelBookingId)
                ->lists('BookingId')
                ->toArray();
            $channelBookingId = $channelBooking->ChannelBookingId;
            $inventoryDeprecate = false;

            $response = $common->calculateBookingAmounts($channelBookingId, $bookingIds, $inventoryDeprecate);
            if ((int)$channelBooking->BookingStatus = 4) {
                $cancel->sendBookingStatusChangeEmailQueue(
                    $response['hotel'],
                    $response['zuzuBookings'],
                    $response['zuzuBookings']->first(),
                    $response['invoiceDetails'],
                    $response['channelBookings'],
                    $response['paid']
                );

                $cancelEmail[] = $channelBooking->ChannelBookingId;
            } elseif ((int)$channelBooking->BookingStatus = 1) {
                $confirm->sendBookingConfirmationEmailQueue(
                    $response['hotel'],
                    $response['zuzuBookings'],
                    $response['channelBookings'],
                    $response['invoiceDetails']
                );
                $confirmEmail[] = $channelBooking->ChannelBookingId;
            } else {
                $noEmail[] = $channelBooking->ChannelBookingId;
            }
        }

        dd($noEmail, $confirmEmail, $cancelEmail);
    }

    private function sendBookingEmail($contacts, $hotel, $booking)
    {
        $hotelContent = $this->hotelRepo->getHotelContentByHotelId($hotel->Id);
        $subject = trans('messages.email_subject_booking_confirm', ['hotel' => $hotelContent->DisplayName]);

        $hotelContent->hotelName = $hotel->Name;
        $hotelContent->city = $this->cityRepo->getCityById($hotel->CityID)->city_name;
        $hotelContent->telephone_display = $hotel->ContactNoDialCode . " " . $hotel->ContactNo;
        $hotelContent->telephone = $hotel->ContactNoDialCode . $hotel->ContactNo;
        $hotelContent->country_code = $hotel->CountryCode;
        $booking->qr_code = QR_CODE_STORE_PATH . base64_encode($booking->id) . '.' . QR_CODE_FORMAT;

        $ID_lang_code = strtolower(config('customconfig.accessible_sub_domain_default_language.ID'));

        foreach ($contacts as $contact) {
            App::setLocale($contact['locale']);

            $addRealHotelName = (
                    $contact['user'] && $ID_lang_code != strtolower($contact['locale']) &&
                    strtolower($hotelContent->country_code) != strtolower(env('INDONESIA_COUNTRY_CODE'))
                ) || (
                !$contact['user']
                );

            $originalName = $addRealHotelName ? "(" . $hotelContent->hotelName . ")" : "";

            $blade = $contact['user'] ?
                'frontend.emails.booking_confirmation' : 'frontend.emails.booking_confirmation_admin_email';
            Mail::send(
                $blade,
                [
                    'hotel' => $hotelContent->toArray(),
                    'booking' => $booking->toArray(),
                    'cancellation' => $contact['cancellation'] ? true : false,
                    'user' => $contact,
                    'check_in_date' => localeDate($booking['checkin_date'], MYSQL_WEEK_MONTH_DATE_YEAR_FORMAT),
                    'check_out_date' => localeDate($booking['checkout_date'], MYSQL_WEEK_MONTH_DATE_YEAR_FORMAT),
                    'original_hotel_name' => $originalName,
                    'resend' => false
                ],
                function ($m) use ($contact, $hotel, $subject, $booking) {
                    $m->to(
                        $contact['email'],
                        $contact['name']
                    )->subject($subject);
                }
            );
        }
    }

    public function getSendBookingModificationEmail(Request $request)
    {
        $id = (int)$request->get('bookingId');
        $x = $this->bookingRepo->bookingUpdateNotifyEmail([$id]);
        dd('done', $x);
    }

    public function getSetPaymentMethods()
    {
        try {
            set_time_limit(0);
            DB::beginTransaction();
            /*
             * TWOC2P hotels
             */
            $newPaymentMethods_twoC2p = [];
            $oldPaymentMethods_twoC2p = [];
            $tcHotels = HotelModel::where('TwoCTwoP', 1)->get();
            $hotelPayments = HotelPaymentMethodsModel::whereIn('HotelID', $tcHotels->lists('Id')->toArray())->get();
            foreach ($tcHotels as $hotel) {
                $hp = $hotelPayments->where('HotelID', $hotel->Id)->first();

                if (!$hp) {
                    $newPaymentMethods_twoC2p[] = $hotel->Id . "|" . $hotel->Slug;
                    $hp = new HotelPaymentMethodsModel();
                    $hp->HotelID = $hotel->Id;
                    $hp->BankName = 'NA';
                    $hp->BankAddress = 'NA';
                    $hp->BankCountry = 'NA';
                    $hp->BankCity = 'NA';
                    $hp->AccountName = 'NA';
                    $hp->SWIFTCode = 'NA';
                    $hp->BranchID = 'NA';
                    $hp->BankAccountNumber = 'NA';
                    $hp->Other = 'NA';
                    $hp->CreatedDate = Carbon::now()->toDateTimeString();
                    $hp->ModifiedDate = Carbon::now()->toDateTimeString();
                } else {
                    $oldPaymentMethods_twoC2p[] = $hotel->Id . "|" . $hotel->Slug;
                }

                $hp->PaymentMethod = SetHotelPaymentModel::$twoc2pId;
                $hp->Dcc = $hotel->Dcc;
                $hp->PaymentFrequent = SetHotelPaymentModel::$perProcessFrequentId;
                $hp->PerProcessCharge = 0.00;

                $hp->save();
            }

            /*
             * NON-TWOC2P HOTELS
             */
            $newPaymentMethods_NON_twoC2p = [];
            $oldPaymentMethods_NON_twoC2p = [];
            $nonTcHotels = HotelModel::where('TwoCTwoP', '!=', 1)->limit(10)->get();
            $hotelPayments_nonTwoC2P =
                HotelPaymentMethodsModel::whereIn('HotelID', $nonTcHotels->lists('Id')->toArray())->get();
            foreach ($nonTcHotels as $hotel_1) {
                $hp = $hotelPayments_nonTwoC2P->where('HotelID', $hotel_1->Id)->first();

                if (!$hp) {
                    $newPaymentMethods_NON_twoC2p[] = $hotel_1->Id . "|" . $hotel_1->Slug;
                    $hp = new HotelPaymentMethodsModel();
                    $hp->HotelID = $hotel_1->Id;
                    $hp->BankName = 'NA';
                    $hp->BankAddress = 'NA';
                    $hp->BankCountry = 'NA';
                    $hp->BankCity = 'NA';
                    $hp->AccountName = 'NA';
                    $hp->SWIFTCode = 'NA';
                    $hp->BranchID = 'NA';
                    $hp->BankAccountNumber = 'NA';
                    $hp->Other = 'NA';
                    $hp->CreatedDate = Carbon::now()->toDateTimeString();
                    $hp->ModifiedDate = Carbon::now()->toDateTimeString();
                } else {
                    $oldPaymentMethods_NON_twoC2p[] = $hotel_1->Id . "|" . $hotel_1->Slug;
                }

                $hp->PaymentMethod = 3;
                $hp->PaymentFrequent = 2;
                $hp->PerProcessCharge = 0.00;

                $hp->save();
            }
            DB::commit();

            dd([
                'new_2c2p_enabled' => $newPaymentMethods_twoC2p,
                'altered_2c2p_enabled' => $oldPaymentMethods_twoC2p,
                'new_2c2p_disabled' => $newPaymentMethods_NON_twoC2p,
                'altered_2c2p_disabled' => $oldPaymentMethods_NON_twoC2p,
            ]);
        } catch (Exception $e) {
            DB::rollBack();
            dd($e);
        }
    }

    /**
     * Update coupon code amount field.
     */
    public function getUpdateCouponAmount()
    {
        $bookingUpdated = [];
        $bookingNotUpdated = [];
        $bookings = BookingModel::whereNotNull('BookingCouponCodeID')
            ->whereNull('PriceModalCouponAmount')
            ->get();

        foreach ($bookings as $k => $booking) {
            $priceModalActualTotal = ($booking->PriceModelClientTaxAmount * 100) / $booking->HotelTaxRate;
            $priceModalCouponAmount = ($priceModalActualTotal +
                    $booking->PriceModelClientTaxAmount) - $booking->PriceModelClientTotalAmount;

            $hotelModalActualTotalSGD = ($booking->BookingHotelTaxAmount * 100) / $booking->HotelTaxRate;
            $hotelModalCouponAmount = floor(($hotelModalActualTotalSGD +
                        $booking->BookingHotelTaxAmount) * $booking->CurrencyRate * 10) / 10;
            $hotelModalCouponAmount = $hotelModalCouponAmount - $booking->ClientTotalAmount;

            $updated = BookingModel::where('ID', $booking->ID)
                ->update([
                    'ClinetCouponAmount' => $hotelModalCouponAmount,
                    'PriceModalCouponAmount' => $priceModalCouponAmount
                ]);

            if ($updated) {
                $bookingUpdated[] = [
                    'ID' => $booking->ID,
                    'ReservationNumber' => $booking->ReservationNumber
                ];
            } else {
                $bookingNotUpdated[] = [
                    'ID' => $booking->ID,
                    'ReservationNumber' => $booking->ReservationNumber
                ];
            }
        }

        dd($bookingUpdated, $bookingNotUpdated);
    }

    public function getUpdateUserLanguage()
    {
        set_time_limit(0);
        $users = UserModel::whereNull('language_id')
            ->get();
        $success = [];
        $unSuccess = [];

        foreach ($users as $key => $user) {
            $getBooking = BookingModel::where('BookingUserEmail', $user->email)
                ->where('PaymentStatus', 1)
                ->first();

            if (!$getBooking) {
                $getBooking = BookingModel::where('BookingUserEmail', $user->email)
                    ->first();
            }

            if ($getBooking && $getBooking->UserLanguageID) {
                $return = UserModel::where('email', $user->email)
                    ->update([
                        'language_id' => $getBooking->UserLanguageID
                    ]);
            } else {
                $return = false;
            }

            if ($return) {
                $success[] = $user->id;
            } else {
                $unSuccess[] = $user->id;
            }
        }

        dd($success, $unSuccess);
    }

    public function getUpdateHotelRatePlan()
    {
        set_time_limit(0);

        try {
            $loggedUser = new \stdClass();
            $loggedUser->id = 1;

            $hotels = HotelModel::join('trn_RoomTypeRate', 'trn_RoomTypeRate.HotelID', '=', 'trn_Hotel.Id')
                ->groupBy('trn_Hotel.Id')
                ->where('trn_Hotel.Status', 1)
                ->where('trn_Hotel.Id', '>', 118)
                ->limit(20)
                ->lists('trn_Hotel.Id');

            foreach ($hotels as $key => $hotel) {
                $existDefaultHotelRatePlan = HotelRatePlanModel::where('HotelId', $hotel)
                    ->where('Default', IS_DEFAULT)
                    ->first();

                $roomTypesRatesManaged = RoomTypeRateModel::where('HotelID', $hotel)
                    ->join(
                        'set_PredefinedHotelRoomTypeContent',
                        'set_PredefinedHotelRoomTypeContent.HotelRoomTypeID',
                        '=',
                        'trn_RoomTypeRate.RoomTypeID'
                    )
                    ->where('set_PredefinedHotelRoomTypeContent.LanguageID', DEFAULT_LANGUAGE_ID)
                    ->get();

                if ($existDefaultHotelRatePlan) {
                    $existDefaultHotelRatePlanContent = HotelRatePlanContentModel::where(
                        'HotelRatePlanId',
                        $existDefaultHotelRatePlan->Id
                    )
                        ->where('LanguageId', DEFAULT_LANGUAGE_ID)
                        ->first();

                    foreach ($roomTypesRatesManaged as $k => $rooms) {
                        $roomTypeAddedToRatePlan = HotelRatePlanRoomsModel::where('HotelRoomTypeId', $rooms->RoomTypeID)
                            ->where('HotelRatePlanId', $existDefaultHotelRatePlan->Id)
                            ->first();

                        if ($roomTypeAddedToRatePlan) {
                        } else {
                            HotelRatePlanRoomsModel::insert([
                                'HotelRatePlanId' => $existDefaultHotelRatePlan->Id,
                                'HotelRoomTypeId' => $rooms->RoomTypeID,
                                'RoomRatePlanName' => ($existDefaultHotelRatePlanContent->HotelRatePlanName . ' - ' .
                                    $rooms->HotelRoomTypeName),
                                'RatePlanRoomSoldPercentage' => 100
                            ]);
                        }

                        RatePlan::updateRatesForRatePlan(
                            [$existDefaultHotelRatePlan->Id],
                            $loggedUser,
                            [$rooms->RoomTypeID]
                        );
                    }
                } else {
                    $now = Carbon::now();
                    $defaultRatePlanSetting = RatePlanModel::join(
                        'set_RatePlanContent',
                        'set_RatePlanContent.RatePlanId',
                        '=',
                        'set_RatePlan.Id'
                    )
                        ->where('Default', IS_DEFAULT)
                        ->first();

                    if (!$defaultRatePlanSetting) {
                        return;
                    }

                    $rateValue = 0;

                    // Add default rate plan for the hotel.
                    $ratePlanInserted = HotelRatePlanModel::insertGetId([
                        'RatePlanId' => $defaultRatePlanSetting->Id,
                        'HotelId' => $hotel,
                        'RateOperation' => $defaultRatePlanSetting->RateOperation,
                        'RateOption' => $defaultRatePlanSetting->RateOption,
                        'RateValue' => $rateValue,
                        'ExtraGuestRateOption' => $defaultRatePlanSetting->ExtraGuestRateOption,
                        'ExtraGuestRate' => $defaultRatePlanSetting->ExtraGuestRate,
                        'ThroughoutYears' => 1,
                        'DurationFrom' => $now->toDateString(),
                        'DurationTo' => $now->addYear(HotelRatePlanModel::$settings['DefaultRatePlanDuration'])
                            ->toDateString(),
                        'AdvancePurchaseDayPrior' => $defaultRatePlanSetting->AdvancePurchaseDayPrior,
                        'MinimumNights' => $defaultRatePlanSetting->MinimumNights,
                        'Status' => ACTIVE_STATUS,
                        'Order' => 1,
                        'Default' => IS_DEFAULT,
                        'CreatedBy' => 1,
                        'CreatedDate' => date(DATETIME_MYSQL_FORMAT)
                    ]);

                    if ($ratePlanInserted) {
                        HotelRatePlanContentModel::insert([
                            'HotelRatePlanId' => $ratePlanInserted,
                            'LanguageId' => DEFAULT_LANGUAGE_ID,
                            'HotelRatePlanName' => $defaultRatePlanSetting->RatePlan,
                            'HotelRatePlanDescription' => $defaultRatePlanSetting->Description
                        ]);

                        foreach ($roomTypesRatesManaged as $k => $rooms) {
                            HotelRatePlanRoomsModel::insert([
                                'HotelRatePlanId' => $ratePlanInserted,
                                'HotelRoomTypeId' => $rooms->RoomTypeID,
                                'RoomRatePlanName' => ($defaultRatePlanSetting->RatePlan . ' - ' .
                                    $rooms->HotelRoomTypeName),
                                'RatePlanRoomSoldPercentage' => 100
                            ]);

                            RatePlan::updateRatesForRatePlan(
                                [$ratePlanInserted],
                                $loggedUser,
                                [$rooms->RoomTypeID]
                            );
                        }
                    }
                }
            }

            dd('All Done');
        } catch (\Exception $e) {
            dd($e);
        }
    }


    public function getUpdatePaymentCurrencyRate()
    {
        $rows = Booking2C2PModel::whereNull('ConversionRate')
            ->get();

        foreach ($rows as $key => $row) {
            $rate = 1;
            if ($row->HotelProcessChargeAmount) {
                $hpca = explode(',', $row->HotelProcessChargeAmount);
                $rate = (float)filter_var($hpca[1], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
            }

            if ($rate == 1) {
                $getBooking = BookingModel::where('ReservationNumber', $row->ReservationNumber)
                    ->first();
                if ($getBooking->HotelCurrencyCode == $row->Currency) {
                    $rate = $getBooking->HotelCurrencyRate;
                } else {
                    $currency = CurrencyModel::where('CurrencyCode', $row->Currency)
                        ->first();
                    $rate = $currency->CurrencyRate;
                }
            }

            $row->ConversionRate = $rate;
            $row->save();
        }

        dd('Done');
    }

    public function getUpdateOriginalPaymentCurrencyRate()
    {
        $rows = Booking2C2PModel::whereNull('OriginalCurrencyConversionRate')
            ->get();

        foreach ($rows as $key => $row) {
            $rate = 1;
            if ($row->HotelProcessChargeAmount) {
                $hpca = explode(',', $row->HotelProcessChargeAmount);
                $rate = (float)filter_var(end($hpca), FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
            }

            if ($rate == 1) {
                $getBooking = BookingModel::where('ReservationNumber', $row->ReservationNumber)
                    ->first();
                if ($getBooking->HotelCurrencyCode == $row->OriginalCurrency) {
                    $rate = $getBooking->HotelCurrencyRate;
                } else {
                    $currency = CurrencyModel::where('CurrencyCode', $row->OriginalCurrency)
                        ->first();
                    $rate = $currency ? $currency->CurrencyRate : 1;
                }
            }

            $row->OriginalCurrencyConversionRate = $rate;
            $row->save();
        }

        dd('Done');
    }

    public function getUpdateHotelBookingAmountCalc()
    {
        try {
            $bookings = BookingModel::whereNull('HotelTotalAmount')
                ->get();
            $defaultCurrency = CurrencyModel::join(
                'sys_CurrencyContent',
                'sys_CurrencyContent.CurrencyID',
                '=',
                'sys_Currency.Id'
            )
                ->where('sys_Currency.Id', DEFAULT_CURRENCY_ID)
                ->where('sys_CurrencyContent.LanguageID', DEFAULT_LANGUAGE_ID)
                ->first();

            foreach ($bookings as $key => $booking) {
                $startDate = Carbon::createFromFormat(DATETIME_MYSQL_FORMAT, $booking->BookingCheckinDate);
                $endDate = Carbon::createFromFormat(DATETIME_MYSQL_FORMAT, $booking->BookingCheckoutDate);
                $rate = 0;
                $dayRate = 0;

                do {
                    switch ($startDate->dayOfWeek) {
                        case Carbon::MONDAY:
                            $dayRate = $booking->HotelRoomTypeMonRate;
                            break;
                        case Carbon::TUESDAY:
                            $dayRate = $booking->HotelRoomTypeTueRate;
                            break;
                        case Carbon::WEDNESDAY:
                            $dayRate = $booking->HotelRoomTypeWedRate;
                            break;
                        case Carbon::THURSDAY:
                            $dayRate = $booking->HotelRoomTypeThuRate;
                            break;
                        case Carbon::FRIDAY:
                            $dayRate = $booking->HotelRoomTypeFriRate;
                            break;
                        case Carbon::SATURDAY:
                            $dayRate = $booking->HotelRoomTypeSatRate;
                            break;
                        case Carbon::SUNDAY:
                            $dayRate = $booking->HotelRoomTypeSunRate;
                            break;
                    }

                    if ($booking->BookingGuestCount > $booking->HotelRoomTypeOccupancy) {
                        if ($booking->HotelRoomTypeAdditionalGuestRateOption == 0) {
                            $additionalGuestRate = ($dayRate * ($booking->HotelRoomTypeAdditionalGuestRate / 100)) *
                                ($booking->BookingGuestCount - $booking->HotelRoomTypeOccupancy);
                        } else {
                            $additionalGuestRate = ($booking->HotelRoomTypeAdditionalGuestRate) *
                                ($booking->BookingGuestCount - $booking->HotelRoomTypeOccupancy);
                        }
                    } else {
                        $additionalGuestRate = 0;
                    }

                    $rate += ($dayRate + $additionalGuestRate);

                    $startDate = $startDate->addDay();
                } while ($startDate->toDateString() < $endDate->toDateString());

                $total = $rate * $booking->BookingRoomsCount;
                $commission = $total * ($booking->HotelCommissionRate / 100);
                $tax = ($total + $commission) * ($booking->HotelTaxRate / 100);

                BookingModel::where('ID', $booking->ID)->update([
                    'HotelTotalAmount' => ($total + $commission + $tax),
                    'HotelTaxAmount' => $tax,
                    'HotelCommissionAmount' => $commission,
                    'HotelCurrencyID' => $defaultCurrency->Id,
                    'HotelCurrencyName' => $defaultCurrency->CurrencyName,
                    'HotelCurrencyCode' => $defaultCurrency->CurrencyCode,
                    'HotelCurrencySymbol' => $defaultCurrency->CurrencySymbol,
                    'HotelCurrencyRate' => $defaultCurrency->CurrencyRate
                ]);
            }

            return $bookings;
        } catch (\Exception $e) {
            dd($e);
        }
    }

    public function getUpdateChannelBookingDetails()
    {
        $ids = [];

        $channelBooking = ChannelBookingModel::join(
            'trn_Booking',
            'trn_Booking.ID',
            '=',
            'trn_ChannelBooking.BookingId'
        )
            ->whereNull('trn_ChannelBooking.OTAChannelId')
            ->get();

        foreach ($channelBooking as $key => $booking) {
            // Get channel
            $channel = ChannelMapModel::join(
                'set_OTAChannelContent',
                'set_OTAChannelContent.ChannelId',
                '=',
                'trn_ChannelGenreMapping.OTAChannelId'
            )
                ->join(
                    'trn_OTAHotelChannel',
                    'trn_OTAHotelChannel.OTAChannelId',
                    '=',
                    'set_OTAChannelContent.ChannelId'
                )
                ->where('trn_ChannelGenreMapping.OTAChannelMapName', $booking->Company)
                ->where('trn_ChannelGenreMapping.ChannelId', $booking->ChannelId)
                ->where('trn_OTAHotelChannel.HotelId', $booking->HotelID)
                ->where('set_OTAChannelContent.LanguageId', DEFAULT_LANGUAGE_ID)
                ->first();

            if ($channel) {
                ChannelBookingModel::where('BookingId', $booking->BookingId)
                    ->where('ChannelBookingId', $booking->ChannelBookingId)
                    ->where('ChannelId', $booking->ChannelId)
                    ->where('Company', $booking->Company)
                    ->update([
                        'OTAChannelId' => $channel->OTAChannelId,
                        'OTAChannelName' => $channel->ChannelName,
                        'OTANatureOfDistribution' => $channel->NatureOfDistribution,
                        'OTAChannelCommission' => $channel->ChannelCommission,
                        'OTAChannelCommissionApplicableOn' => $channel->ChannelCommissionApplicableOn,
                        'OTATravellerPaymentToChannel' => $channel->TravellerPaymentToChannel,
                        'OTAPerformanceFee' => $channel->PerformanceFee,
                        'OTAPerformanceFeeOption' => $channel->PerformanceFeeOption,
                        'OTAPerformanceFeeApplicableOn' => $channel->PerformanceFeeApplicableOn,
                        'OTAChannelInvoiceHandling' => $channel->ChannelInvoiceHandling,
                        'OTAChannelStatus' => $channel->ChannelInvoiceHandling
                    ]);

                $ids[] = $booking->BookingId;
            }
        }

        dd($ids);
    }

    public function getSetZuzuHotelsChannel()
    {
        ini_set('max_execution_time', 600);
        ini_set('memory_limit', '2048M');

        $added = [];
        $bookings = BookingModel::leftJoin(
            'trn_ChannelBooking',
            'trn_Booking.ID',
            '=',
            'trn_ChannelBooking.BookingId'
        )
            ->whereNull('trn_ChannelBooking.BookingId')
            ->get();

        foreach ($bookings as $booking) {
            $booking->ZHBookingType = 2;
            $booking->save();

            $zuzuHotelChannel = HotelOTAChannelModel::select(
                'trn_OTAHotelChannel.*',
                'set_OTAChannels.Name AS Name'
            )
                ->join(
                    'set_OTAChannels',
                    'set_OTAChannels.Id',
                    '=',
                    'trn_OTAHotelChannel.OTAChannelId'
                )
                ->where('set_OTAChannels.Default', IS_DEFAULT)
                ->where('trn_OTAHotelChannel.HotelId', $booking->HotelID)
                ->first();

            $channelBooking = new ChannelBookingModel();
            $channelBooking->BookingId = $booking->ID;
            $channelBooking->ChannelBookingId = $booking->ReservationNumber;
            $channelBooking->Company = $zuzuHotelChannel->Name;
            $channelBooking->ChannelCommission = $booking->HotelCommissionAmount;
            $channelBooking->ChannelTax = $booking->HotelTaxAmount;

            $channelBooking->OTAChannelId = $zuzuHotelChannel->OTAChannelId;
            $channelBooking->OTAChannelName = $zuzuHotelChannel->Name;
            $channelBooking->OTAChannelCommission = $zuzuHotelChannel->ChannelCommission;
            $channelBooking->OTAChannelCommissionApplicableOn = $zuzuHotelChannel->ChannelCommissionApplicableOn;
            $channelBooking->OTATravellerPaymentToChannel = $zuzuHotelChannel->TravellerPaymentToChannel;

            $channelBooking->OTAPerformanceFee = $zuzuHotelChannel->PerformanceFee;
            $channelBooking->OTAPerformanceFeeOption = $zuzuHotelChannel->PerformanceFeeOption;
            $channelBooking->OTAPerformanceFeeApplicableOn = $zuzuHotelChannel->PerformanceFeeApplicableOn;
            $channelBooking->OTAChannelInvoiceHandling = $zuzuHotelChannel->ChannelInvoiceHandling;
            $channelBooking->OTAChannelStatus = $zuzuHotelChannel->ChannelStatus;

            $channelBooking->save();

            $added[] = $booking->ID;
        }

        dd($added);
    }

    public function getUpdateHotelDefaultChannel()
    {
        $hotels = HotelModel::lists('trn_Hotel.Id');

        foreach ($hotels as $hotel) {
            $this->hotelOTAChannelRepo->addNewOtaToHotel($hotel);
        }
    }

    public function getUpdateBookingStayDates()
    {
        $bookings = BookingModel::leftJoin(
            'trn_BookingStayDates',
            'trn_BookingStayDates.BookingId',
            '=',
            'trn_Booking.ID'
        )
            ->whereNull('trn_BookingStayDates.BookingId')
            ->get();

        $array = [];

        foreach ($bookings as $booking) {
            $checkedIn = Carbon::parse($booking->BookingCheckinDate);
            $checkedOut = Carbon::parse($booking->BookingCheckoutDate);

            do {
                $dayRate = 0;

                switch ($checkedIn->dayOfWeek) {
                    case Carbon::MONDAY:
                        $dayRate = $booking->HotelRoomTypeMonRate;
                        break;
                    case Carbon::TUESDAY:
                        $dayRate = $booking->HotelRoomTypeTueRate;
                        break;
                    case Carbon::WEDNESDAY:
                        $dayRate = $booking->HotelRoomTypeWedRate;
                        break;
                    case Carbon::THURSDAY:
                        $dayRate = $booking->HotelRoomTypeThuRate;
                        break;
                    case Carbon::FRIDAY:
                        $dayRate = $booking->HotelRoomTypeFriRate;
                        break;
                    case Carbon::SATURDAY:
                        $dayRate = $booking->HotelRoomTypeSatRate;
                        break;
                    case Carbon::SUNDAY:
                        $dayRate = $booking->HotelRoomTypeSunRate;
                        break;
                }

                $array[] = [
                    'BookingId' => $booking->ID,
                    'StayDate' => $checkedIn->toDateString(),
                    'RoomTypeRate' => $dayRate
                ];

                $checkedIn->addDay();
            } while ($checkedIn->lt($checkedOut));
        }

        BookingStayDatesModel::insert($array);
    }

    public function getUpdateStopSell()
    {
        $hotelsAndRooms = HotelRoomTypeDetailedAvailabilityModel::where('Date', '>=', '2017-03-10')
            ->where('Availability', 0);

        $hotelsAndRoomsC = clone $hotelsAndRooms;
        $hotelsAndRoomsA = $hotelsAndRoomsC->groupBy('HotelID')->groupBy('RoomTypeID')->get();
        $s = [];

        foreach ($hotelsAndRoomsA as $roomHotel) {
            $filter = clone $hotelsAndRooms;

            $updated = HotelRatesRawModel::where('HotelID', $roomHotel->HotelID)
                ->where('RoomTypeID', $roomHotel->RoomTypeID)
                ->whereIn('Date', $filter
                    ->where('HotelID', $roomHotel->HotelID)
                    ->where('RoomTypeID', $roomHotel->RoomTypeID)
                    ->lists('Date')
                    ->toArray())
                ->update([
                    'StopSell' => 1
                ]);

            if ($updated) {
                $s [] = [
                    'h' => $roomHotel->HotelID,
                    'r' => $roomHotel->RoomTypeID
                ];
            }
        }

        dd($s);
    }

    public function getUpdateChannelRefNo()
    {
        $channelBookings = ChannelBookingModel::whereNull('ChannelRefId')
            ->where('OTAChannelId', '!=', 4)
            ->get();

        foreach ($channelBookings as $channel) {
            $xml = simplexml_load_string($channel->Response);
            $json = json_encode($xml);
            $array = json_decode($json, true);

            ChannelBookingModel::whereNull('ChannelRefId')
                ->where('BookingId', $channel->BookingId)
                ->where('ChannelBookingId', $channel->ChannelBookingId)
                ->update([
                    'ChannelRefId' => $array['reservation']['channel_ref']
                ]);
        }

        dd($channelBookings);
    }

    public function getUpdatePriceModalClientAmount()
    {
        $bookings = BookingModel::whereNotIn('trn_Booking.ZHBookingType', [1, 2])
            ->whereNotNull('trn_ChannelBooking.ChannelPaymentMapOptions')
            ->join('trn_ChannelBooking', 'trn_ChannelBooking.BookingId', '=', 'trn_Booking.ID')
            ->get();

        $modified = [];
        $all = [];

        dd();

        foreach ($bookings as $booking) {
            $mapping = unserialize($booking->ChannelPaymentMapOptions);
            $mappingCollection = collect($mapping);
            $foundTotalPriceMap = $mappingCollection->where('Map', 'totalprice')->first();
            $value = null;
            $all[] = $booking->ID;

            if ($foundTotalPriceMap) {
                switch ($foundTotalPriceMap->MethodId) {
                    case 1:
                        $value = null;
                        break;
                    case 2:
                        $bookingTaxAmount = $booking->PriceModelClientTotalAmount * $booking->HotelTaxRate / 100;
                        $value = $bookingTaxAmount + $booking->PriceModelClientTotalAmount;
                        break;
                    case 3:
                        $bookingTotalAmountExclTax = $booking->PriceModelClientTotalAmount * 100 /
                            $booking->HotelTaxRate;
                        $value = $bookingTotalAmountExclTax + $booking->PriceModelClientTotalAmount;
                        break;
                    case 4:
                        if ($booking->OTAChannelCommissionApplicableOn == 2) {
                            $value = $booking->PriceModelClientTotalAmount * ($booking->HotelTaxRate + 100) /
                                $booking->OTAChannelCommission;
                        } else {
                            $value = $booking->PriceModelClientTotalAmount * 100 / $booking->OTAChannelCommission;
                        }
                        break;
                    case 5:
                        if ($booking->OTAChannelCommissionApplicableOn == 2) {
                            $value = $booking->PriceModelClientTotalAmount * 100 * (100 + $booking->HotelTaxRate) /
                                ((100 + $booking->HotelTaxRate) * (100 - $booking->OTAChannelCommission) +
                                    ($booking->OTAChannelCommission * $booking->HotelTaxRate));
                        } else {
                            $value = $booking->PriceModelClientTotalAmount * 100 /
                                (100 - $booking->OTAChannelCommission);
                        }
                        break;
                    default:
                        $value = null;
                        break;
                }

                if ($value) {
                    BookingModel::where('ID', $booking->ID)
                        ->update([
                            'PriceModelClientTotalAmount' => $value,
                            'PriceModelClientTaxAmount' => getTax(
                                $value * 100 / (100 + $booking->HotelTaxRate),
                                $booking->HotelTaxRate
                            ),
                            'PriceModelTotalAmount' => $value / $booking->CurrencyRate,
                            'PriceModelTaxAmount' => getTax(($value / $booking->CurrencyRate * 100 /
                                (100 + $booking->HotelTaxRate)), $booking->HotelTaxRate)
                        ]);
                    $modified[] = $booking->ID;
                }
            }
        }

        dd($all, $modified);
    }

    public function getUpdateRevisedHotelTotalDue()
    {
        $bookingsNeedsToUpdate = BookingModel::whereIn('BookingStatus', [
            BOOKING_CHECK_IN,
            BOOKING_NO_SHOW,
            BOOKING_CANCELLED
        ])
            ->whereIn('ZHBookingType', [4, 8, 6, 5])
            ->get();

        dd($bookingsNeedsToUpdate->count());

        $updated = [];
        $failed = [];
        $noNeedUpdated = [];
        $hasMultiple = [];

        foreach ($bookingsNeedsToUpdate as $booking) {
            $paymentPaidHotel = Booking2C2PModel::where('ReservationNumber', $booking->ReservationNumber)
                ->get();

            if ($paymentPaidHotel->count() == 1) {
                $paymentPaidHotel = $paymentPaidHotel->first();

                // Booking invoice calculations
                $calculatedAmount = BookingInvoiceData::getBookingInvoiceInformation($booking->ID);

                if ((float)$paymentPaidHotel->OriginalAmount == $calculatedAmount['net_amount_owed_hotel']) {
                    $noNeedUpdated[] = $booking->ReservationNumber;
                } else {
                    $response = Booking2C2PModel::where('ReservationNumber', $booking->ReservationNumber)
                        ->update([
                            'OriginalAmount' => $calculatedAmount['net_amount_owed_hotel'],
                            'Amount' => $calculatedAmount['net_amount_owed_hotel']
                        ]);

                    if ($response) {
                        $updated[] = $booking->ReservationNumber;
                    } else {
                        $failed[] = $booking->ReservationNumber;
                    }
                }
            } else {
                $hasMultiple[] = $booking->ReservationNumber;
            }
        }

        dd($updated, $failed, $noNeedUpdated, $hasMultiple);
    }

    public function getUpdateBookingAmounts()
    {
        $bookings = BookingModel::join('trn_ChannelBooking', 'trn_ChannelBooking.BookingId', '=', 'trn_Booking.ID')
            ->where('trn_ChannelBooking.OTAChannelId', '!=', 4)
            ->whereNull('trn_ChannelBooking.ChannelPaymentMapOptions')
            ->get();

        dd($bookings->count());

        $hasMultiple = [];
        $updated = [];
        $failed = [];

        foreach ($bookings as $booking) {
            $payload = $booking->Response;
            $xml = simplexml_load_string($payload);
            $json = json_encode($xml);
            $array = json_decode($json, true);

            if (!is_array(array_values($array['reservation']['room'])[0])) {
                $array['reservation']['room'] = [$array['reservation']['room']];
            }

            $array['reservation']['actual_company'] = $array['reservation']['company'];
            $map = $this->channelRepo->getMappedOtaSource($array['reservation']['company'], 1);

            if ($map) {
                $array['reservation']['company'] = $map->Name;
            }

            $bookedRoom = [];
            $count = 0;

            foreach ($xml->reservation->room as $room) {
                $formatted = [];
                foreach ($room->price as $price) {
                    if (array_key_exists($price['rate_id']->__toString(), $formatted)) {
                        $formatted[$price['rate_id']->__toString()][] = $price['date']->__toString();
                    } else {
                        $formatted[$price['rate_id']->__toString()] = [$price['date']->__toString()];
                    }
                }

                $c = 0;
                foreach ($room->price as $key => $price) {
                    $arrKey = $room->arrival_date . '_' . $room->departure_date . "_" . $room->room_id . "_" .
                        $price['rate_id'];

                    if (array_key_exists($arrKey, $bookedRoom)) {
                        $bookedRoom[$arrKey]['totalprice'] += $price->__toString();
                        $bookedRoom[$arrKey]['stay_date_price'][$price['date']->__toString()] = $price->__toString();
                    } else {
                        $bookedRoom[$arrKey] = [];
                        foreach ($room as $k => $r) {
                            $bookedRoom[$arrKey][$k] = $r->__toString();
                        }

                        $bookedRoom[$arrKey]['stay_date_price'][$price['date']->__toString()] = $price->__toString();
                        $rateId = $formatted[$price['rate_id']->__toString()];
                        $bookedRoom[$arrKey]['rate_plan'] = $price['rate_id']->__toString();
                        $bookedRoom[$arrKey]['totalprice'] = $price->__toString();
                        $bookedRoom[$arrKey]['arrival_date'] = $rateId[0];
                        $bookedRoom[$arrKey]['departure_date'] = Carbon::parse(end($rateId))->addDay()->toDateString();
                        $bookedRoom[$arrKey]['numberofguests'] = $price['numberofadult']->__toString();
                        $bookedRoom[$arrKey]['numberofchild'] = $price['numberofchild']->__toString();
                    }

                    if (!array_key_exists('number_of_rooms', $bookedRoom[$arrKey])) {
                        $bookedRoom[$arrKey]['number_of_rooms'] = 1;
                    } elseif ($c == 0) {
                        $bookedRoom[$arrKey]['number_of_rooms'] += 1;
                    }

                    $c++;
                }

                $array['reservation']['room'] = $bookedRoom;
                $count++;
            }

            if (count($array['reservation']['room']) > 1) {
                $hasMultiple[] = $booking->ReservationNumber;
            } else {
                $hotel = HotelModel::where('Id', $booking->HotelID)->first();
                $map = DB::table('trn_ChannelGenreMapping')
                    ->where('OTAChannelId', $booking->OTAChannelId)
                    ->where('ChannelId', 1)
                    ->first();
                $channelPaymentMapOptionsCollect = OTAChannelPaymentMethodMapModel::where('OTAMapId', $map->Id)->get();
                $channelMapSettings = new \stdClass();
                $channelMapSettings->ChannelCommission = $booking->OTAChannelCommission;
                $channelMapSettings->ChannelCommissionApplicableOn = $booking->OTAChannelCommissionApplicableOn;

                $hotel->Tax = $booking->HotelTaxRate;
                $hotel->Commission = $booking->HotelCommissionRate;
                $room = reset($array['reservation']['room']);
                $bookingTotalAmount = $room['totalprice'];
                $channelCommissionAmount = $array['reservation']['commissionamount'];

                $totalAmountMapFind = $channelPaymentMapOptionsCollect->where('Map', 'totalprice')->first();
                $totalAmountMapId = $totalAmountMapFind ? $totalAmountMapFind->MethodId : 1;

                $function = 'findHotelTotalAmount' . $totalAmountMapId;
                $hotelAmountData = BookingInvoiceData::$function(
                    $bookingTotalAmount,
                    $room,
                    $hotel,
                    $channelMapSettings
                );

                // Channel commission map find
                $channelCommissionAmountMapFind = $channelPaymentMapOptionsCollect
                    ->where('Map', 'commissionamount')
                    ->first();
                if ($channelCommissionAmountMapFind) {
                    $hotelAmountData['actual_channel_commission_amount'] = $channelCommissionAmount;
                } else {
                    $hotelAmountData['actual_channel_commission_amount'] = $channelCommissionAmount > 0 ?
                        $channelCommissionAmount : null;
                }

                $hotelTotalAmount = $hotelAmountData['booking_total_including_tax'];
                $commission = $hotelAmountData['booking_markup_amount'];
                $hotelTaxAmount = $hotelAmountData['booking_tax_amount'];

                $clientTotalAmount = $hotelAmountData['booking_total_including_tax'] /
                    $booking->HotelCurrencyRate * $booking->CurrencyRate;

                $totalAmountSGD = $hotelAmountData['booking_total_including_tax'] / $booking->HotelCurrencyRate;
                $taxAmountSGD = $hotelAmountData['booking_tax_amount'] / $booking->HotelCurrencyRate;
                $commissionAmountSGD = $hotelAmountData['booking_markup_amount'] / $booking->HotelCurrencyRate;


                $priceModelTotalAmount = $hotelAmountData['booking_total_including_tax'] /
                    $booking->CurrencyRate;

                $paymentTable = Booking2C2PModel::where('ReservationNumber', $booking->ReservationNumber)
                    ->first();
                $updatePayment = $paymentTable && $paymentTable->Currency == $paymentTable->OriginalCurrency;

                try {
                    DB::beginTransaction();
                    $response = BookingModel::where('ID', $booking->ID)->update([
                        'BookingTotalAmount' => round($totalAmountSGD),
                        'BookingHotelTaxAmount' => $taxAmountSGD,
                        'BookingHotelCommissionAmount' => round($commissionAmountSGD, PRICE_MAX_DECIMAL),
                        'ClientTotalAmount' => $clientTotalAmount,
                        'PriceModelTotalAmount' => $priceModelTotalAmount,
                        'PriceModelTaxAmount' => getTax(
                            $priceModelTotalAmount * 100 / (100 + $hotel->Tax),
                            $hotel->Tax
                        ),
                        'PriceModelClientTotalAmount' => $hotelAmountData['booking_total_including_tax'],
                        'PriceModelClientTaxAmount' => getTax(
                            $hotelAmountData['booking_total_including_tax'] * 100 / (100 + $hotel->Tax),
                            $hotel->Tax
                        ),
                        'HotelTotalAmount' => round($hotelTotalAmount),
                        'HotelTaxAmount' => $hotelTaxAmount,
                        'HotelCommissionAmount' => $commission
                    ]);

                    if ($response) {
                        $response = ChannelBookingModel::where('BookingId', $booking->ID)
                            ->update([
                                'ActualChannelCommissionAmount' => $hotelAmountData['actual_channel_commission_amount'],
                                'ChannelPaymentMapOptions' => serialize($channelPaymentMapOptionsCollect)
                            ]);

                        if ($response) {
                            foreach ($hotelAmountData['stay_date_price'] as $date => $amount) {
                                $response = BookingStayDatesModel::where('BookingId', $booking->ID)
                                    ->where('StayDate', $date)
                                    ->update([
                                        'RoomTypeRate' => $amount
                                    ]);

                                if (!$response) {
                                    break;
                                }
                            }

                            if ($response) {
                                DB::commit();

                                if ($updatePayment) {
                                    $calculatedAmount = BookingInvoiceData::getBookingInvoiceInformation(
                                        $booking->ID
                                    );
                                    Booking2C2PModel::where(
                                        'ReservationNumber',
                                        $booking->ReservationNumber
                                    )
                                        ->update([
                                            'OriginalAmount' => $calculatedAmount['net_amount_owed_hotel'],
                                            'Amount' => $calculatedAmount['net_amount_owed_hotel']
                                        ]);
                                }

                                $updated[] = [$booking->ReservationNumber, $updatePayment];
                            } else {
                                DB::rollback();
                                $failed[] = $booking->ReservationNumber;
                            }
                        } else {
                            DB::rollback();
                            $failed[] = $booking->ReservationNumber;
                        }
                    } else {
                        DB::rollback();
                        $failed[] = $booking->ReservationNumber;
                    }
                } catch (Exception $e) {
                    DB::rollback();
                    dd($e);
                }
            }
        }

        dd($updated, $failed, $hasMultiple);
    }

    /**
     * Delete bookings.
     */
    public function getDeleteBookings()
    {
        if (!request()->get("id")) {
            dd("no id");
        }
        $reservationIds = [request()->get("id")];

        $rows = $this->bookingRepo->deleteBooking($reservationIds);

        dd($rows);
    }

    public function getUpdateChannelCommission()
    {
        $ids = [
        ];

        $bookings = BookingModel::whereIn('trn_Booking.ReservationNumber', $ids)
            ->join('trn_ChannelBooking', 'trn_ChannelBooking.BookingId', '=', 'trn_Booking.ID')
            ->get();

        $updated = [];
        $failed = [];
        $noMultiples = [];
        $commission0 = [];

        foreach ($bookings as $booking) {
            $payload = $booking->Response;
            $xml = simplexml_load_string($payload);
            $json = json_encode($xml);
            $array = json_decode($json, true);

            $channelBookings = BookingModel::where(
                'trn_ChannelBooking.ChannelBookingId',
                $array['reservation']['booking_id']
            )
                ->join('trn_ChannelBooking', 'trn_ChannelBooking.BookingId', '=', 'trn_Booking.ID')
                ->get();

            if ($channelBookings->count() > 1) {
                if ($array['reservation']['commissionamount'] != 0) {
                    $channelCommission = $array['reservation']['commissionamount'] *
                        $booking->PriceModelClientTotalAmount / $channelBookings->sum('PriceModelClientTotalAmount');

                    $response = ChannelBookingModel::where(
                        'BookingId',
                        $booking->ID
                    )
                        ->update([
                            'ChannelCommission' => $channelCommission,
                            'ActualChannelCommissionAmount' => $channelCommission
                        ]);

                    if ($response) {
                        $updated[] = $booking->ReservationNumber;
                    } else {
                        $failed[] = $booking->ReservationNumber;
                    }

                } else {
                    $commission0[] = $booking->ReservationNumber;
                }
            } else {
                $noMultiples[] = $booking->ReservationNumber;
            }
        }

        dd($updated, $failed, $commission0, $noMultiples);
    }

    public function getUpdateChannelBookingCreated()
    {
        set_time_limit(0);
        $bookings = BookingModel::select(
            'trn_ChannelBooking.*'
        )
            ->join('trn_ChannelBooking', 'trn_ChannelBooking.BookingId', '=', 'trn_Booking.ID')
            ->where('trn_ChannelBooking.OTAChannelId', '!=', 4)
            ->whereNotIn('trn_Booking.ZHBookingType', [1, 2])
            ->whereNotNull('trn_Booking.ZHBookingType')
            ->get();

        $updated = [];
        $failed = [];

        foreach ($bookings as $booking) {
            $payload = $booking->Response;
            $xml = simplexml_load_string($payload);
            $json = json_encode($xml);
            $array = json_decode($json, true);

            $xmlCreatedDate = Carbon::parse($array['reservation']['booking_date']);

            $response = BookingModel::where('ID', $booking->BookingId)
                ->update([
                    'CreatedDate' => $xmlCreatedDate->toDateTimeString()
                ]);

            if ($response) {
                $updated[] = $booking->BookingId;
            } else {
                $failed[] = $booking->BookingId;
            }
        }
        dd($updated, $failed);
    }

    public function getCheckRPCalendarAvailability()
    {
        $booking = BookingModel::find(4726);

        if ($booking->BookingStatus == BOOKING_CONFIRMED || $booking->BookingStatus == BOOKING_CANCELLED) {
            dd('YES', $booking->BookingStatus, BOOKING_CONFIRMED);
        } else {
            dd('NO', $booking->BookingStatus, BOOKING_CONFIRMED, $booking->BookingStatus == BOOKING_CONFIRMED);
        }
    }

    public function getUpdateRPCalendarAvailability()
    {
        set_time_limit(0);
        $now = Carbon::now();

        $bookings = BookingModel::where(
            DB::raw('DATE_FORMAT(BookingCheckinDate, "' . MYSQL_CONDITION_DATE_FORMAT . '")'),
            '>=',
            $now->toDateString()
        )
            ->whereNotNull('HotelRatePlanId')
            ->groupBy('HotelID')
            ->groupBy('HotelRoomTypeID')
            ->groupBy('HotelRatePlanId')
            ->whereIn('ReservationNumber', [])
            ->get();

//        $booking = new \stdClass();
//        $booking->HotelID = 369;
//        $booking->HotelRoomTypeID= 12;
//        $booking->HotelRatePlanId= 419;
//        $booking->BookingCheckinDate = '2017-07-01 00:00:00';
//        $booking->BookingCheckoutDate = '2017-07-02 00:00:00';
//        $booking->ID = null;
//
//        $bookings = [$booking];

        dd($bookings);

        $r = [];

        foreach ($bookings as $booking) {
            $rPs = HotelRatePlanModel::where('HotelId', $booking->HotelID)->get();

            foreach ($rPs as $rP) {
                $ratePlan = HotelRatePlanModel::where('Id', $rP->Id)->first();
                $totalBookingsCollect = BookingModel::select(
                    '*',
                    DB::raw('DATE_FORMAT(BookingCheckinDate, "' . MYSQL_CONDITION_DATE_FORMAT . '") AS BookingCheckinDate'),
                    DB::raw('DATE_FORMAT(BookingCheckoutDate, "' . MYSQL_CONDITION_DATE_FORMAT . '") AS BookingCheckoutDate')
                )
                    ->where('HotelRoomTypeID', $booking->HotelRoomTypeID)
                    ->where('HotelID', $booking->HotelID)
                    ->where(function ($q) {
                        $q->where('BookingStatus', '!=', BOOKING_CANCELLED);
                        $q->where('BookingStatus', '!=', BOOKING_NOT_CONFIRMED);
                        $q->where('BookingStatus', '!=', BOOKING_PAYMENT_FAILURE);
                        $q->where('BookingStatus', '!=', BOOKING_NO_SHOW);
                        $q->where('BookingStatus', '!=', BOOKING_NO_SHOW_CONFIRMED);
                    })
                    ->where(
                        DB::raw('DATE_FORMAT(BookingCheckinDate, "' . MYSQL_CONDITION_DATE_FORMAT . '")'),
                        '>=',
                        $now->toDateString()
                    )
                    ->get();

                // Pick hotel rate plans
                $hotelPlans = HotelRatePlanModel::select(
                    'trn_HotelRatePlan.*',
                    'set_RatePlan.Default'
                )
                    ->join(
                        'set_RatePlan',
                        'set_RatePlan.Id',
                        '=',
                        'trn_HotelRatePlan.RatePlanId'
                    )
                    ->where('HotelId', $ratePlan->HotelId)
                    ->get();

                try {
                    if ($ratePlan->ThroughoutYears) {
                        $ratePlan->DurationFrom = clone $now;
                        $nowCloned = clone $now;
                        $ratePlan->DurationTo = $nowCloned
                            ->addYear(HotelRatePlanModel::$settings['DefaultRatePlanDuration']);

                        $ratePlan->DurationFrom = Carbon::parse($booking->BookingCheckinDate);
                        $ratePlan->DurationTo = Carbon::parse($booking->BookingCheckoutDate);

                        $this->availabilityUpdate($ratePlan, $booking, $totalBookingsCollect, $now, $hotelPlans);
                    } else {
                        $dateSlots = HotelRatePlanDurationModel::where('HotelRatePlanId', $ratePlan->Id)
                            ->where(function ($w) use ($now) {
                                $w->whereRaw('"' . $now->toDateString() . '" BETWEEN DurationFrom AND DurationTo')
                                    ->orWhere('DurationFrom', '>=', $now->toDateString());
                            })
                            ->get();

                        foreach ($dateSlots as $dateSlot) {
                            $ratePlan->DurationFrom = Carbon::createFromFormat(
                                DATE_MYSQL_FORMAT_SHORT,
                                $dateSlot->DurationFrom
                            );
                            $ratePlan->DurationTo = Carbon::createFromFormat(
                                DATE_MYSQL_FORMAT_SHORT,
                                $dateSlot->DurationTo
                            );

                            $ratePlan->DurationFrom = Carbon::parse($booking->BookingCheckinDate);
                            $ratePlan->DurationTo = Carbon::parse($booking->BookingCheckoutDate);

                            $this->availabilityUpdate($ratePlan, $booking, $totalBookingsCollect, $now, $hotelPlans);
                        }
                    }

                    $r[] = [
                        'HotelID' => $booking->HotelID,
                        'HotelRoomTypeID' => $booking->HotelRoomTypeID,
                        'HotelRatePlanId' => $booking->HotelRatePlanId,
                        'ID' => $booking->ID
                    ];
                } catch (Exception $e) {
                    dd($e, $r);
                }
            }
        }

        dd($r);
    }

    private function availabilityUpdate($rP, $b, $tB, $now, $hP)
    {
        $startDate = $rP->DurationFrom;
        $endDate = $rP->DurationTo;

        if (!$startDate->gte($now)) {
            $startDate = clone $now;
        }

        $ratesCollect = HotelRatesRawModel::where('HotelId', $rP->HotelId)
            ->where('RoomTypeId', $b->HotelRoomTypeID)
            ->get();

        $default = $hP->where('Default', IS_DEFAULT)
            ->first();

        do {
            $rateRaw = $ratesCollect
                ->where('RatePlanId', $rP->Id)
                ->where('Date', $startDate->toDateString())
                ->first();

            $dA = $ratesCollect
                ->where('Date', $startDate->toDateString())
                ->where('RatePlanId', $default->Id)
                ->first();

            $totalBookingsRoomOnly = $tB->filter(function ($item) use ($startDate) {
                return (data_get($item, 'BookingCheckinDate') <= $startDate->toDateString()) &&
                    (data_get($item, 'BookingCheckoutDate') > $startDate->toDateString());
            })->sum('BookingRoomsCount');

            $totalBookings = $tB->filter(function ($item) use ($startDate) {
                return (data_get($item, 'BookingCheckinDate') <= $startDate->toDateString()) &&
                    (data_get($item, 'BookingCheckoutDate') > $startDate->toDateString());
            })->where('HotelRatePlanId', $rP->Id)
                ->sum('BookingRoomsCount');


            if ($rateRaw && $dA) {
                $hotelAvailabilityDefaultPlan = $dA->Availability;

                $hotelDefaultPlanActualAvailability = $hotelAvailabilityDefaultPlan - $totalBookingsRoomOnly;
                $hotelPlanActualAvailability = $rateRaw->Availability - $totalBookings;

                $actualAvailability = $hotelDefaultPlanActualAvailability > $hotelPlanActualAvailability ?
                    $hotelPlanActualAvailability :
                    $hotelDefaultPlanActualAvailability;

                if ($rateRaw->DefaultAvailability == IS_DEFAULT) {
                    $update = [
                        'TotalBookings' => $totalBookings,
                        'ActualAvailability' => $actualAvailability > 0 ? $actualAvailability : 0
                    ];
                } elseif ($rateRaw->DefaultAvailability != IS_DEFAULT) {
                    $update = [
                        'TotalBookings' => $totalBookings
                    ];
                } else {
                    $update = null;
                }

                if ($update) {
                    HotelRatesRawModel::where('HotelId', $rP->HotelId)
                        ->where('RoomTypeId', $b->HotelRoomTypeID)
                        ->where('RatePlanId', $rP->Id)
                        ->where('Date', $startDate->toDateString())
                        ->update($update);
                }
            }

            $startDate = $startDate->addDay();
        } while ($startDate->toDateString() <= $endDate->toDateString());
    }

    public function getUpdateStaahAvailability()
    {
        set_time_limit(0);
        $hotelIds = [
            20,
            46,
            104,
            114,
            118,
            133,
            135,
            142,
            156,
            159,
            207,
            218,
            223,
            272,
            289,
            314,
            351,
            352,
            353,
            356,
            358,
            359,
            360,
            361,
            362,
            363,
            366,
            368,
            369,
            370,
            373
        ];

        $now = Carbon::now();

        $bookings = BookingModel::where(
            DB::raw('DATE_FORMAT(BookingCheckinDate, "' . MYSQL_CONDITION_DATE_FORMAT . '")'),
            '>=',
            $now->toDateString()
        )
            ->whereNotNull('HotelRatePlanId')
            ->groupBy('HotelID')
            ->groupBy('HotelRoomTypeID')
            ->groupBy('HotelRoomTypeID', [20, 46, 104, 114, 118])
            ->get();

        dd($bookings);

        foreach ($bookings as $booking) {
            $this->channelRepo->updateChannelAvailabilitiesAndRates($booking->HotelID, $booking->HotelRoomTypeID);
        }

        dd($hotelIds);
    }

    /**
     * Update hotel BASE rates to BAR rates.
     */
    public function getBaseToBar()
    {
        set_time_limit(0);
        $hotelIds = [];

        try {
            $hotels = HotelModel::where("OnBar", 0)
                ->where("Status", ACTIVE_STATUS)
                ->limit(5)
                ->get();

            dd($hotels);

            $hotelIds = [];
            foreach ($hotels as $hotel) {
                if ($hotel->OnBar == 0) {
                    DB::beginTransaction();

                    $commonQ = "(((100+{$hotel->Tax})*(100+{$hotel->Commission}))/10000)";

                    //percentage rate variance option
                    $percentagePlus = "((RateCalculated*100/(100+trn_HotelRatePlan.RateValue))
                    *{$commonQ}*
                    ((100+trn_HotelRatePlan.RateValue)/100))";
                    $percentageMinus = "((RateCalculated*100/(100-trn_HotelRatePlan.RateValue))
                    *{$commonQ}*
                    ((100-trn_HotelRatePlan.RateValue)/100))";

                    //value rate variance option
                    $staticPlus = "(((RateCalculated-trn_HotelRatePlan.RateValue)*{$commonQ}) +
                    (trn_HotelRatePlan.RateValue*{$commonQ}))";
                    $staticMinus = "(((RateCalculated+trn_HotelRatePlan.RateValue)*{$commonQ}) -
                    (trn_HotelRatePlan.RateValue*{$commonQ}))";

                    $percentageRateVariance = "IF(
                        trn_HotelRatePlan.RateOperation=1,
                        {$percentagePlus},
                        {$percentageMinus}
                    )";
                    $staticRateVariance = "IF(trn_HotelRatePlan.RateOperation=1, {$staticPlus}, {$staticMinus})";

                    $rateCalculateQuerySub = "IF(
                        trn_HotelRatePlan.RateOption = 0,
                        {$percentageRateVariance},
                        {$staticRateVariance}
                    )";

                    $rateCalculateQuery = "IF(
                        trn_HotelRoomAvailabilityAndRates.DefaultRate=1,
                        {$rateCalculateQuerySub},
                        ((RateCalculated*(100+{$hotel->Tax})*(100+{$hotel->Commission}))/10000)
                    )";

                    $updateQueryCalender = "UPDATE trn_HotelRoomAvailabilityAndRates
                    INNER JOIN trn_HotelRatePlan ON trn_HotelRatePlan.Id = trn_HotelRoomAvailabilityAndRates.RatePlanId
                    SET trn_HotelRoomAvailabilityAndRates.RateCalculated = CEIL({$rateCalculateQuery}),
                    trn_HotelRatePlan.RateValue = IF(
                        trn_HotelRatePlan.RateOption=0,
                        trn_HotelRatePlan.RateValue,
                        trn_HotelRatePlan.RateValue*{$commonQ}
                    )
                    WHERE trn_HotelRoomAvailabilityAndRates.HotelId = {$hotel->Id};";

                    $updateQueryRoomTypeRate = "UPDATE trn_RoomTypeRate
                    SET MonRate = CEIL((MonRate * (100 + {$hotel->Tax}) * (100 + {$hotel->Commission})) / 10000),
                    TueRate = CEIL((TueRate * (100 + {$hotel->Tax}) * (100 + {$hotel->Commission})) / 10000),
                    WedRate = CEIL((WedRate * (100 + {$hotel->Tax}) * (100 + {$hotel->Commission})) / 10000),
                    ThuRate = CEIL((ThuRate * (100 + {$hotel->Tax}) * (100 + {$hotel->Commission})) / 10000),
                    FriRate = CEIL((FriRate * (100 + {$hotel->Tax}) * (100 + {$hotel->Commission})) / 10000),
                    SatRate = CEIL((SatRate * (100 + {$hotel->Tax}) * (100 + {$hotel->Commission})) / 10000),
                    SunRate = CEIL((SunRate * (100 + {$hotel->Tax}) * (100 + {$hotel->Commission})) / 10000),
                    AdditionalGuestRate = IF(
                        MultiplierType=1,
                        CEIL((AdditionalGuestRate * (100 + {$hotel->Tax}) * (100 + {$hotel->Commission})) / 10000),
                        AdditionalGuestRate
                    )
                    WHERE
                        HotelID =  {$hotel->Id};";

                    $calender = DB::update($updateQueryCalender);
                    $typeRate = DB::update($updateQueryRoomTypeRate);

                    $hotel->Commission = 0;
                    $hotel->OnBar = 1;
                    $hotel->save();

                    DB::commit();

                    $hotelIds[] = [
                        "ID" => $hotel->Id,
                        "Slug" => $hotel->Slug,
                        "Name" => $hotel->Name,
                        "Calender" => $calender,
                        "TypeRate" => $typeRate
                    ];

                    ChannelConnect::updateRatesAndAvailabilities(
                        $hotel->Id
                    );
                }
            }

            echo "success <br />";

            $table = "<table style='border: 1px solid black;  border-collapse: collapse;'>";
            foreach ($hotelIds as $hotel) {
                $table .= "<tr>";

                $table .= "<td style='border: 1px solid black; padding: 5px'>" . $hotel['ID'] . "</td>";
                $table .= "<td style='border: 1px solid black; padding: 5px'>" . $hotel['Name'] . "</td>";
                $table .= "<td style='border: 1px solid black; padding: 5px'>" . $hotel['Slug'] . "</td>";
                $table .= "<td style='border: 1px solid black; padding: 5px'>" . $hotel['Calender'] . "</td>";
                $table .= "<td style='border: 1px solid black; padding: 5px'>" . $hotel['TypeRate'] . "</td>";

                $table .= "</tr>";
            }
            $table .= "</table>";

            echo $table;

            dd("done");
        } catch (Exception $e) {
            DB::rollBack();

            return [
                "success" => false,
                "hotels" => $hotelIds
            ];
        }
    }

    public function getExcBookings()
    {
        $requestId = uniqid('ZUZU-', true);
        dd();
        ini_set('max_execution_time', 600);

        // $q = 'SELECT * FROM	trn_ChannelBookingXML WHERE	Id >= 21601 AND Id <= 21614;';
        $q = 'SELECT * FROM	trn_ChannelBookingXML WHERE	Id IN (21071, 21072);';
        $bookingsXmls = DB::select($q);


        $ids = [];

        $html = "<table style='border: 1px solid black;  border-collapse: collapse;'>";
        foreach ($bookingsXmls as $booking) {
            $added = StaahConnect::notifyBooking($requestId, $booking->Xml);

            if ($added === true) {
                $x = simplexml_load_string($booking->Xml);

                $channelBooking = ChannelBookingModel::where("ChannelBookingId", $x->reservation->booking_id->__toString())
                    ->where("ChannelRefId", $x->reservation->channel_ref->__toString())
                    ->first();

                if ($channelBooking) {
                    $html .= "<tr>" .
                        "<td style='border: 1px solid black; padding: 5px'>{$x->reservation->booking_id->__toString()}</td>" .
                        "<td style='border: 1px solid black; padding: 5px'>{$x->reservation->channel_ref->__toString()}</td>" .
                        "<td style='border: 1px solid black; padding: 5px'>{$x->reservation->booking_status->__toString()}</td>" .
                        "<td style='border: 1px solid black; padding: 5px'>{$channelBooking->BookingId}</td>" .
                        "</tr>";

                    $ids[] = $channelBooking->BookingId;
                }
            } else {
                $html .= "</table>";
                dd($added, $html);
            }
        }

        $html .= "</table>";

        return $html;
    }

    public function getExecutedBookingIds()
    {
        dd();
        $ids = [
            10867, 10867, 10867, 10867, 10867, 10871, 10872, 10871, 10872, 10871, 10872, 10871, 10872, 10871, 10872, 10881,
            10881, 10881, 10881, 10881, 10882, 10883, 10882, 10883, 10882, 10883, 10882, 10883, 10882, 10883, 8928, 8928,
            8928, 8928, 8928, 10629, 10629, 10629, 10629, 10885, 10629, 10885, 10885, 10885, 10885, 9267, 9267, 9267, 9267,
            9267, 10886, 10886, 10886, 10887, 10886, 10887, 10886, 10887, 10887, 10887, 10888, 10888, 10888, 10888, 10888,
            9851, 10889, 9851, 10889, 9851, 10889, 9851, 10889, 9851, 9909, 9909, 9909, 9909, 9909, 10892, 10892, 10892,
            10043, 10892, 10043, 10892, 10043, 10043, 10043, 9868, 9868, 9868, 9868, 9868, 10893, 10893, 10894, 10893, 10894,
            10894, 10894, 10894, 10895, 10895, 10895, 10895, 10895, 10896, 10897, 10898, 10896, 10897, 10898, 10896, 10897,
            10898, 10896, 10897, 10898, 10896, 10894, 10894, 10894, 10894, 10894, 10899, 10899, 10899, 10900, 10900, 10900,
            9187, 9187, 9187, 9187, 9187, 10901, 10902, 10903, 10901, 10902, 10903, 10901, 10902, 10903, 10901, 10902, 10903,
            10904, 10901, 10902, 10903, 10904, 10497, 10497, 10497, 10497, 10497, 10906, 10906, 10906, 8107, 8107, 8107, 8107,
            8107, 10909, 10909, 10909, 8125, 9019, 8125, 9019, 8125, 9019, 8125, 9019, 10911, 8125, 9019, 10912, 10912, 10911,
            10911, 10911, 10911, 7955, 7955, 10914, 7955, 10914, 7955, 10914, 7955, 10914, 10914, 9956, 9956, 9956, 9956, 9956,
            10784, 10784, 10916, 10784, 10916, 10784, 10916, 10784, 10916, 10916, 10485, 10485, 10485, 10485, 10652, 10653,
            10485, 10652, 10653, 10652, 10653, 10652, 10653, 10652, 10653, 10825, 10825, 8847, 8851, 10654, 10825, 8847, 8851,
            10654, 10825, 8847, 8851, 10654, 10825, 8847, 8851, 10654, 8847, 8851, 10654, 10919, 10919, 10919, 10919, 10920,
            10919, 10920, 10920, 10920, 10920, 10437, 10437, 10437, 10439, 10437, 10439, 10437, 10439, 10439, 10439, 10921,
            10620, 10620, 10620, 10620, 10620, 10255, 10255, 10255, 10255, 10255, 10923, 10923, 10923, 10923, 10923, 10925,
            10925, 10925, 10640, 10640, 10640, 10640, 10640, 5955, 5955, 5955, 5955, 5955, 10832, 10832, 10832, 10832, 10832,
            10927, 10927, 10927, 10127, 10127, 10127, 10127, 10127, 8749, 8749, 8749, 8749, 8749, 10215, 10215, 10215, 10215,
            10215, 10906, 10906, 10906, 10906, 10906, 10782, 10782, 10782, 10928, 10782, 10928, 10782, 10928, 10928, 10928,
            10930, 10930, 10932, 10932, 10932, 10865, 10865, 10865, 10865, 10865, 10859, 10859, 10859, 10859, 10942, 10945,
            10946, 10947, 10948, 10949, 10950, 10951, 10952, 10953, 10954, 10955, 10956, 10958, 10959, 10960, 10930, 10932,
            10961, 10942, 10963, 10965, 10966, 10967, 10969, 10971, 10972, 10973, 10925, 10974, 10975, 10927, 10976, 10977,
            10978, 10978, 10979, 10980, 10981, 10983, 10984, 10985, 10988, 10988, 10889, 10889, 10989, 10990, 10990, 10991,
            10992, 10993, 10994, 10995, 10996, 10997, 11000, 11000, 11001, 11002, 11003, 10988, 10988, 11004, 11004, 11005,
            11005, 10893, 11007, 11008, 11008, 11009, 11011, 11012, 11013, 11014, 11015, 11016, 11016, 10897, 10898, 11017,
            11019, 11020, 11021, 11022, 11004, 11004, 10900, 10900, 10899, 11033, 11034, 11035, 11037, 11038, 11038, 11039,
            11040, 11015, 11041, 11043, 11044, 11044, 11016, 11016, 10904, 11045, 11046, 11047, 11048, 11048, 11049, 11050,
            11051, 11053, 11054, 11055, 11055, 11056, 11057, 11058, 11059, 11059, 11060, 11062, 11063, 10969, 11064, 11065,
            11066, 11067, 11068, 11070, 10919, 10919, 10921, 10921, 11071, 11072, 11055, 11055, 11040, 10906, 10906, 11073,
            11074, 11075, 11076, 11077, 11078, 11078, 11079, 11080, 10909, 11081, 11081, 11082, 11083, 11084, 11085, 11086,
            10912, 10912, 11088, 11088
        ];


        dd(BookingModel::select("ReservationNumber")->whereIn("ID", array_unique($ids))->get()->lists('ReservationNumber'));
    }

    public function getExportMerSubscribers()
    {
        set_time_limit(0);

        $users = $this->userRepo->getAllUsers([
            ROLE_FRONTEND_USER_CHILD,
            ROLE_FRONTEND_USER
        ], true);

        return Excel::create('user_list', function ($excel) use ($users) {
            $excel->sheet('ZUZU Registered users', function ($sheet) use ($users) {
                $sheet->loadView('frontend.todo.export_mer_subscribers')
                    ->with('data', $users);

                $sheet->setWidth('A', 20);
                $sheet->setWidth('B', 30);
                $sheet->setWidth('C', 30);
                $sheet->setWidth('D', 30);
                $sheet->setWidth('E', 30);
                $sheet->setWidth('F', 30);
            });
        })->download('xls');
    }

    public function getChannelSettings()
    {
        $otaChannels = DB::table("set_OTAChannels")->get();
        foreach ($otaChannels as $setting) {
            DB::table("set_CountryOtaChannels")->insert([
                "SetOtaChannelId" => $setting->Id,
                "ChannelCommission" => $setting->ChannelCommission,
                "ChannelCommissionApplicableOn" => $setting->ChannelCommissionApplicableOn,
                "TravellerPaymentToChannel" => $setting->TravellerPaymentToChannel
            ]);
        }
        dd($otaChannels);
    }

    /**
     * Check channel xmls.
     */
    public function getFindChannels()
    {
        $channel = request("channel", "");
        dd();
        $model = new ChannelBookingXMLModel();
        $table = "<table style='border: 1px solid black;  border-collapse: collapse;'>";
        $model
            ->where('Xml', 'LIKE', '%' . $channel . '%')
            ->orderBy("CreatedDate", "DESC")
            ->chunk(200, function ($inspectors) use (&$table) {
                foreach ($inspectors as $inspector) {
                    try {
                        $xml = simplexml_load_string($inspector->Xml);

                        $table .= "<tr>";
                        $table .= "<td style='border: 1px solid black; padding: 5px'>{$inspector->Id}</td>";
                        $table .= "<td style='border: 1px solid black; padding: 5px'>{$xml->reservation->channel_ref->__toString()}</td>";
                        $table .= "<td style='border: 1px solid black; padding: 5px'>{$xml->reservation->booking_id->__toString()}</td>";
                        $table .= "<td style='border: 1px solid black; padding: 5px'>{$xml->reservation->customer->email->__toString()}</td>";
                        $table .= "<td style='border: 1px solid black; padding: 5px'>{$inspector->CreatedDate}</td>";

                        $table .= "</tr>";

                    } catch (Exception $e) {
                    }
                }
            });

        $table .= "</table>";

        echo $table;
    }

    /**
     * Update traveloka bookings.
     */
    public function getUpdateTravelokaBookings()
    {
        dd('--');
        $getBookings = BookingModel::select(
            'trn_Booking.ID',
            'trn_Booking.IsBarBooking',
            'trn_ChannelBooking.Response',
            'trn_Booking.ReservationNumber',
            'trn_Booking.HotelID',
            'trn_Booking.CurrencyRate',
            'trn_Booking.HotelCurrencyRate',
            'trn_Booking.HotelTotalAmount',
            'trn_Booking.HotelTaxAmount',
            'trn_Booking.HotelCommissionAmount',
            'trn_Booking.ClientTotalAmount',
            'trn_Booking.BookingTotalAmount',
            'trn_Booking.BookingHotelTaxAmount',
            'trn_Booking.BookingHotelCommissionAmount',
            'trn_Booking.PriceModelTotalAmount',
            'trn_Booking.PriceModelClientTotalAmount',
            'trn_Booking.PriceModelClientTaxAmount',
            'trn_ChannelBooking.ActualChannelCommissionAmount'
        )
            ->join('trn_ChannelBooking', 'trn_ChannelBooking.BookingId', '=', 'trn_Booking.ID')
            ->whereIn('trn_Booking.ReservationNumber', [
            ])->get();

        $updated = [];
        $multipleBookings = [];
        $error = [];

        foreach ($getBookings as $booking) {
            $hotel = HotelModel::where('Id', $booking->HotelID)->first();

            $xml = simplexml_load_string($booking->Response);
            $json = json_encode($xml);
            $staahBooking = json_decode($json, true);

            if (!is_array(array_values($staahBooking['reservation']['room'])[0])) {
                $staahBooking['reservation']['room'] = [$staahBooking['reservation']['room']];
            }

            $bookedRoom = [];

            foreach (simplexml_load_string($booking->Response)->reservation->room as $room) {
                $formatted = [];
                foreach ($room->price as $price) {
                    if (array_key_exists($price['rate_id']->__toString(), $formatted)) {
                        $formatted[$price['rate_id']->__toString()][] = $price['date']->__toString();
                    } else {
                        $formatted[$price['rate_id']->__toString()] = [$price['date']->__toString()];
                    }
                }

                $c = 0;
                foreach ($room->price as $key => $price) {
                    $arrKey = $room->arrival_date . '_' . $room->departure_date . "_" . $room->room_id . "_" .
                        $price['rate_id'];

                    if (array_key_exists($arrKey, $bookedRoom)) {
                        $bookedRoom[$arrKey]['totalprice'] += $price->__toString();
                        if (isset($bookedRoom[$arrKey]['stay_date_price'][$price['date']->__toString()])) {
                            $bookedRoom[$arrKey]['stay_date_price'][$price['date']->__toString()] +=
                                $price->__toString();
                        } else {
                            $bookedRoom[$arrKey]['stay_date_price'][$price['date']->__toString()] =
                                $price->__toString();
                        }
                    } else {
                        $bookedRoom[$arrKey] = [];
                        foreach ($room as $k => $r) {
                            $bookedRoom[$arrKey][$k] = $r->__toString();
                        }

                        $bookedRoom[$arrKey]['stay_date_price'][$price['date']->__toString()] = $price->__toString();
                        $rateId = $formatted[$price['rate_id']->__toString()];
                        $bookedRoom[$arrKey]['rate_plan'] = $price['rate_id']->__toString();
                        $bookedRoom[$arrKey]['totalprice'] = $price->__toString();
                        $bookedRoom[$arrKey]['arrival_date'] = $rateId[0];
                        $bookedRoom[$arrKey]['departure_date'] = Carbon::parse(end($rateId))->addDay()->toDateString();
                        $bookedRoom[$arrKey]['numberofguests'] = $price['numberofadult']->__toString();
                        $bookedRoom[$arrKey]['numberofchild'] = $price['numberofchild']->__toString();
                    }

                    if (!array_key_exists('number_of_rooms', $bookedRoom[$arrKey])) {
                        $bookedRoom[$arrKey]['number_of_rooms'] = 1;
                    } elseif ($c == 0) {
                        $bookedRoom[$arrKey]['number_of_rooms'] += 1;
                    }

                    $c++;
                }
            }

            $staahBooking['reservation']['room'] = $bookedRoom;
            $staahBooking['reservation']['actual_company'] = $staahBooking['reservation']['company'];

            $map = $this->channelRepo->getMappedOtaSource($staahBooking['reservation']['company'], 1);
            if ($map) {
                $staahBooking['reservation']['company'] = $map->Name;
            }

            $count = count($staahBooking['reservation']['room']);

            if ($count == 1) {
                $room = head($staahBooking['reservation']['room']);

                if ($booking->IsBarBooking == 1) {
                    $hotelAmountData = BookingInvoiceBarData::getBookingAmountDetails(
                        $staahBooking['reservation'],
                        $room,
                        1,
                        $hotel
                    );
                } else {
                    $hotelAmountData = BookingInvoiceData::getBookingAmountDetails(
                        $staahBooking['reservation'],
                        $room,
                        1,
                        $hotel
                    );
                }

                $hotelTotalAmount = $hotelAmountData['booking_total_including_tax'];
                $commission = $hotelAmountData['booking_markup_amount'];
                $hotelTaxAmount = $hotelAmountData['booking_tax_amount'];

                $clientTotalAmount = $hotelAmountData['booking_total_including_tax'] /
                    $booking->HotelCurrencyRate * $booking->CurrencyRate;

                $totalAmountSGD = $hotelAmountData['booking_total_including_tax'] / $booking->HotelCurrencyRate;
                $taxAmountSGD = $hotelAmountData['booking_tax_amount'] / $booking->HotelCurrencyRate;
                $commissionAmountSGD = $hotelAmountData['booking_markup_amount'] / $booking->HotelCurrencyRate;

                $priceModelTotal = $hotelAmountData['booking_total_including_tax'] / $booking->CurrencyRate;

                $update = 'UPDATE trn_Booking' .
                    ' INNER JOIN trn_ChannelBooking ON trn_ChannelBooking.BookingId = trn_Booking.ID' .
                    ' INNER JOIN trn_BookingStayDates ON trn_BookingStayDates.BookingID = trn_Booking.ID' .
                    ' SET trn_Booking.HotelTotalAmount = ' . round($hotelTotalAmount) .
                    ', trn_Booking.HotelTaxAmount = ' . $hotelTaxAmount .
                    ', trn_Booking.HotelCommissionAmount = ' . ($booking->IsBarBooking == 1 ? 0 : $commission) .
                    ', trn_Booking.ClientTotalAmount = ' . $clientTotalAmount .
                    ', trn_Booking.BookingTotalAmount = ' . round($totalAmountSGD) .
                    ', trn_Booking.BookingHotelTaxAmount = ' . $taxAmountSGD .
                    ', trn_Booking.BookingHotelCommissionAmount = ' . ($booking->IsBarBooking == 1 ?
                        0 : round($commissionAmountSGD, PRICE_MAX_DECIMAL)) .
                    ', trn_Booking.PriceModelTotalAmount = ' . $priceModelTotal .
                    ', trn_Booking.PriceModelTaxAmount = ' . getTax(
                        $priceModelTotal * 100 / (100 + $hotel->Tax),
                        $hotel->Tax
                    ) .
                    ', trn_Booking.PriceModelClientTotalAmount = ' . $hotelAmountData['booking_total_including_tax'] .
                    ', trn_Booking.PriceModelClientTaxAmount = ' . getTax(
                        $hotelAmountData['booking_total_including_tax'] * 100 / (100 + $hotel->Tax),
                        $hotel->Tax
                    ) .
                    ', trn_ChannelBooking.ActualChannelCommissionAmount = ' .
                    (is_null($hotelAmountData['actual_channel_commission_amount']) ? 'NULL' :
                        $hotelAmountData['actual_channel_commission_amount']) .
                    ', trn_BookingStayDates.RoomTypeRate = (CASE trn_BookingStayDates.StayDate';

                foreach ($hotelAmountData['stay_date_price'] as $date => $price) {
                    $update .= ' WHEN "' . $date . '" THEN ' . $price;
                }

                $update .= ' END) WHERE trn_Booking.ID = ' . $booking->ID;

                $response = DB::statement($update);

                if ($response) {
                    $updated[] = $booking->ReservationNumber;
                } else {
                    $error[] = $booking->ReservationNumber;
                }
            } else {
                $multipleBookings[] = $booking->ReservationNumber;
            }
        }

        dd($updated, $error, $multipleBookings);
    }

    public function getPopulatePaymentInformation()
    {
        try {
            dd('--');
            set_time_limit(0);
            $bookings = BookingModel::whereIn('ID', [
            ])
                ->get();

            foreach ($bookings as $booking) {
                $invoiceData = $booking->IsBarBooking == 1 ?
                    BookingInvoiceBarData::getBookingInvoiceInformation($booking->ID) :
                    BookingInvoiceData::getBookingInvoiceInformation($booking->ID);

                PaymentData::updateBookingPaymentTables($booking->ID, $invoiceData);
            }

            dd('done');
        } catch (Exception $e) {
            dd($e);
        }
    }


    public function getPopulateCompletedPayments()
    {
        $select = 'SELECT trn_BookingHotelPayment.Id, trn_Booking.ReservationNumber, trn_Booking.ID AS Bid' .
            ', trn_BookingPayments.BookingID, trn_Hotel.Name AS HotelName, trn_Hotel.Id AS HotelID' .
            ', trn_BookingHotelPayment.PaidDate, trn_BookingHotelPayment.Amount AS PaidAmount' .
            ', PaidCurrency.CurrencyCode AS PCurrencyCode,PaidCurrency.Id AS PId' .
            ', PaidCurrency.CurrencySymbol AS PCurrencySymbol, trn_BookingHotelPayment.ConversionRate AS PConversionRate' .
            ', OriginalCurrency.CurrencyCode AS OCurrencyCode, OriginalCurrency.Id AS OId' .
            ', OriginalCurrency.CurrencySymbol AS OCurrencySymbol' .
            ', trn_BookingHotelPayment.OriginalAmount, trn_BookingHotelPayment.OriginalCurrencyConversionRate' .
            ', trn_BookingHotelPayment.AdditionalPercentage, trn_BookingHotelPayment.HotelProcessChargeAmount' .
            ', trn_BookingHotelPayment.ProcessCharge, trn_BookingHotelPayment.CreatedDate, trn_BookingHotelPayment.PaymentFrequent' .
            ' FROM trn_BookingHotelPayment' .
            ' JOIN trn_Booking ON trn_Booking.ReservationNumber = trn_BookingHotelPayment.ReservationNumber' .
            ' JOIN trn_Hotel ON trn_Hotel.Id = trn_Booking.HotelID' .
            ' JOIN sys_Currency AS PaidCurrency ON PaidCurrency.CurrencyCode = trn_BookingHotelPayment.Currency' .
            ' JOIN sys_Currency AS OriginalCurrency ON OriginalCurrency.CurrencyCode = trn_BookingHotelPayment.OriginalCurrency' .
            ' JOIN trn_BookingPayments ON trn_BookingPayments.BookingID = trn_Booking.ID' .
            ' WHERE trn_BookingHotelPayment.PaymentMethod = 1' .
            ' AND trn_BookingPayments.PaymentsMadeToClassificationsId = 1' .
            ' AND trn_BookingPayments.BookingCompletedPaymentId IS NULL' .
            ' AND trn_BookingHotelPayment.PaidDate IS NOT NULL';

        $results = DB::select($select);

        dd($results);

        $completed = [];
        $failed = [];
        $result = '--';

        try {
            DB::beginTransaction();

            foreach ($results as $result) {
                $paidDate = Carbon::parse($result->PaidDate);
                $invoiceCreatedAlreadyCount = InvoiceModel::where('HotelId', $result->HotelID)
                    ->where(DB::raw('DATE_FORMAT(PaidDate, "%Y-%m-%d")'), $paidDate->toDateString())
                    ->count();

                $completePayment = InvoiceModel::insertGetId([
                    'PaymentId' => generateInvoiceId($result, $paidDate, $invoiceCreatedAlreadyCount),
                    'HotelId' => $result->HotelID,
                    'HotelName' => $result->HotelName,
                    'PaymentsMadeToId' => 1,
                    'TotalAmountPaid' => $result->PaidAmount,
                    'PaidCurrency' => $result->PId,
                    'PaidCurrencyCode' => $result->PCurrencyCode,
                    'PaidCurrencySymbol' => $result->PCurrencySymbol,
                    'PaidCurrencyConversionRate' => $result->PConversionRate,
                    'OriginalAmountPaid' => $result->OriginalAmount,
                    'OriginalCurrency' => $result->OId,
                    'OriginalCurrencyCode' => $result->OCurrencyCode,
                    'OriginalCurrencySymbol' => $result->OCurrencySymbol,
                    'OriginalCurrencyConversionRate' => $result->OriginalCurrencyConversionRate,
                    'AdditionalPercentage' => $result->AdditionalPercentage,
                    'HotelProcessChargeAmount' => $result->HotelProcessChargeAmount,
                    'ProcessCharge' => $result->ProcessCharge,
                    'PaymentMethod' => 1,
                    'PaymentFrequent' => $result->PaymentFrequent,
                    'BankName' => null,
                    'AccountName' => null,
                    'BankAccountNumber' => null,
                    'PaidDate' => $paidDate->toDateTimeString(),
                    'PaidStatus' => ACTIVE_STATUS,
                    'CreatedBy' => 1,
                    'CreatedDate' => $result->CreatedDate,
                    'ModifiedBy' => 1
                ]);

                if ($completePayment) {
                    $res = BookingPaymentModel::whereNull('BookingCompletedPaymentId')
                        ->whereIn('PaymentsMadeToClassificationsId', [1, 2])
                        ->where('BookingID', $result->Bid)
                        ->update(['BookingCompletedPaymentId' => $completePayment]);

                    if ($res) {
                        $res = DB::table('trn_BookingCompletedPaymentBreakdown')->insert([
                            'CompletedPaymentID' => $completePayment,
                            'PaymentsMadeToClassificationsId' => 1,
                            'AmountPaidOrReceived' => $result->OriginalAmount,
                            'CreatedBy' => 1,
                            'CreatedDate' => $result->CreatedDate,
                            'ModifiedBy' => 1
                        ]);
                    }

                    if (!$res) {
                        throw new Exception('No update', 1);
                    }

                    $completed[] = $result->Bid;
                } else {
                    $failed[] = $result->Bid;
                }
            }

            DB::commit();
            dd($completed, $failed);
        } catch (Exception $e) {
            DB::rollback();
            dd($completed, $failed, $e, $result);
        }
    }

    /**
     * Fix minus booking counts
     */
    public function getFixMinusSoldCount()
    {
        try {
            dd();
            set_time_limit(0);

            $tb = [];

            $minusSoldRecords = HotelRatesRawModel::where('TotalBookings', "<", 0)->get()->chunk(50);

            foreach ($minusSoldRecords as $rates) {
                foreach ($rates as $rate) {
                    $totalBookings = $this->getBookingCount(
                        $rate->HotelId,
                        $rate->RoomTypeId,
                        $rate->RatePlanId,
                        $rate->Date
                    );

                    $tb[$rate->HotelId . "_" . $rate->RoomTypeId . "_" . $rate->RatePlanId . "_" . $rate->Date] = $totalBookings;

                    HotelRatesRawModel::where("HotelId", $rate->HotelId)
                        ->where("RoomTypeId", $rate->RoomTypeId)
                        ->where("RatePlanId", $rate->RatePlanId)
                        ->where("Date", $rate->Date)
                        ->update(["TotalBookings" => $totalBookings]);
                }
            };
            dd($tb);
        } catch (Exception $e) {
            dd($e);
        }

        dd("done");
    }

    /**
     * Get booking count for given params.
     *
     * @param $hotelId
     * @param $roomTypeId
     * @param $ratePlanId
     * @param $date
     * @return mixed
     */
    private function getBookingCount($hotelId, $roomTypeId, $ratePlanId, $date)
    {
        $eligibleBookingStatuses = [
            BOOKING_CONFIRMED,
            BOOKING_CHECK_IN,
            BOOKING_CHECK_OUT,
            BOOKING_NO_SHOW,
            BOOKING_LAPSED,
            BOOKING_NO_SHOW_CONFIRMED,
            BOOKING_CONFIRMED_MODIFY
        ];

        $bookings = BookingModel::select(DB::raw("SUM(BookingRoomsCount) AS TotalBookings"))
            ->where("HotelID", $hotelId)
            ->where("HotelRoomTypeID", $roomTypeId)
            ->where("HotelRatePlanId", $ratePlanId)
            ->where("BookingCheckinDate", "<=", $date)
            ->where("BookingCheckoutDate", ">", $date)
            ->whereIn("BookingStatus", $eligibleBookingStatuses)
            ->first();

        return $bookings->TotalBookings ?: 0;
    }

    /**
     * Update channel commission percentage for amount passed over by XML.
     */
    public function getUpdateChannelCommissionPercentage()
    {
        set_time_limit(0);

        $channelBookings = ChannelBookingModel::select(
            'trn_Booking.PriceModelClientTotalAmount',
            'trn_Booking.PriceModelClientTaxAmount',
            'trn_ChannelBooking.ActualChannelCommissionAmount',
            'trn_ChannelBooking.OTAChannelCommission',
            'trn_ChannelBooking.OTAChannelCommissionApplicableOn',
            'trn_ChannelBooking.BookingId'
        )
            ->join(
                'trn_Booking',
                'trn_Booking.ID',
                '=',
                'trn_ChannelBooking.BookingId'
            )
            ->whereNotNull('ActualChannelCommissionAmount')
            /*            ->limit(1000)
                        ->offset(0)*/
            ->get();

        $affectedBookings = [];
        $failedBookings = [];

        foreach ($channelBookings as $channelBooking) {
            $grossAmountApplicable = $channelBooking->OTAChannelCommissionApplicableOn ==
            OTAChannelModel::$channelCommissionApplicableMethods['gross_amount_excluding_taxes'] ?
                $channelBooking->PriceModelClientTotalAmount - $channelBooking->PriceModelClientTaxAmount :
                $channelBooking->PriceModelClientTotalAmount;

            $channelCommissionAmountCalc = number_format(
                ($grossAmountApplicable * $channelBooking->OTAChannelCommission) / 100,
                DEFAULT_DECIMAL_NUMBERS
            );

            $actualCommissionAmount = number_format(
                $channelBooking->ActualChannelCommissionAmount,
                DEFAULT_DECIMAL_NUMBERS
            );

            if ($channelCommissionAmountCalc != $actualCommissionAmount) {
                $percentageCalc = ($channelBooking->ActualChannelCommissionAmount * 100 /
                    $grossAmountApplicable);

                $response = ChannelBookingModel::where('BookingId', $channelBooking->BookingId)->update([
                    'OTAChannelCommission' => $percentageCalc
                ]);

                if ($response) {
                    $affectedBookings[$channelBooking->BookingId] = $percentageCalc;
                } else {
                    $failedBookings[$channelBooking->BookingId] = $percentageCalc;
                }
            }
        }

        dd($affectedBookings, $failedBookings);
    }

    public function getCheckSenderEmailChange(Mailer $mailer)
    {
        dd('--');
        changeSenderMail('', '');

        Mail::send([], [], function ($message) {
            $message->to('');
        });

        dd($mailer);
    }

    public function getRevertInventory()
    {
        $reservationIds = [];

        dd($reservationIds);

        $bookings = BookingModel::whereIn("ReservationNumber", $reservationIds)
            ->get();

        $table = "<table style='border: 1px solid black;  border-collapse: collapse;'>";
        foreach ($bookings as $booking) {
            $booking->BookingStatus = BOOKING_CANCELLED;
            $booking->save();
            RatePlan::updateTotalBookingCount($booking);
            $booking->delete();

            $table .= "<tr>";
            $table .= "<td style='border: 1px solid black; padding: 5px'>" . $booking->ID . "</td>";
            $table .= "<td style='border: 1px solid black; padding: 5px'>" . $booking->ReservationNumber . "</td>";
            $table .= "</tr>";
        }

        $table .= "</table>";

        echo $table;
        dd("done");
    }

    public function getRecordStaahBooking()
    {
        $requestId = uniqid('ZUZU-', true);
        $xmlId = request()->get("id");
        dd();
        $xml = ChannelBookingXMLModel::find($xmlId);

        if ($xml) {
            $added = StaahConnect::notifyBooking($requestId, $xml->Xml);

            dd($added);
        } else {
            dd("not found");
        }
    }

    /**
     * Update hotel user table.
     */
    public function getUpdateHotelUser()
    {
        set_time_limit(0);

        try {
            $insertArray = [];
            $now = Carbon::now();

            $query = 'SELECT trn_Hotel.Id AS HotelID, trn_Hotel.Name, trn_Hotel.CreatedDate, trn_Hotel.ContactEmail' .
                ', trn_Users.id AS UserID' .
                ' FROM trn_Hotel' .
                ' INNER JOIN trn_Users ON trn_Users.email = trn_Hotel.ContactEmail' .
                ' LEFT JOIN trn_HotelUser ON trn_HotelUser.HotelID = trn_Hotel.Id' .
                ' WHERE trn_HotelUser.HotelID IS NULL ORDER BY trn_Hotel.CreatedDate ASC';

            $hotels = DB::select($query);

            foreach ($hotels as $hotel) {
                $insertArray[] = [
                    'UserID' => $hotel->UserID,
                    'HotelID' => $hotel->HotelID,
                    'CreatedBy' => 1,
                    'CreatedDate' => $now->toDateTimeString(),
                    'ModifiedDate' => $now->toDateTimeString()
                ];
            }

            HotelUserModel::insert($insertArray);

            dd($insertArray);
        } catch (Exception $e) {
            dd($e);
        }
    }

    public function getUpdateBookingType()
    {
        $bookings = BookingModel::join(
            'trn_ChannelBooking',
            'trn_ChannelBooking.BookingId',
            '=',
            'trn_Booking.ID'
        )
            ->join('trn_Hotel', 'trn_Hotel.Id', '=', 'trn_Booking.HotelID')
            ->join('set_OTAChannels', 'set_OTAChannels.Id', '=', 'trn_ChannelBooking.OTAChannelId')
            ->whereIn('trn_Booking.ReservationNumber', [
                86117245242088,
                65117624663398,
                65117460706423,
                65115953968779,
                65112536546898,
                65117131532649,
                65111524815143,
                65115343980390,
                65116317147859,
                65117015431979,
                65116328003090,
                65117683712380,
                65111836953152,
                65111245334072,
                65117096763169,
                65112393415122,
                65116283709803,
                65112203117707,
                65112245111817,
                65119760801157,
                65119718364340,
                65117812065419,
                65116305227464,
                65113647790636,
                65117850987114,
                65112410463180,
                65117414658050,
                65111956886925,
                65117198540802,
                65114365057243,
                65115078803464,
                65117858191397,
                65115978065128,
                65113396466331,
                65117911375847,
                65119699823867,
                65114636758743,
                65116912113229,
                65112785470932,
                65116629653615,
                65112903939846,
                65116525378406,
                65116950113169,
                65115472755969,
                65118808727547,
                65113672538239,
                65114006713428,
                65113492400121,
                65117793109343,
                65116286521344,
                31114484897383,
                65115159722303,
                65118453618572,
                65118953120980,
                65119308219616,
                65111987686103,
                65118473145980,
                65112098634191,
                65115142713037,
                65111065946442,
                65112625893338,
                65118292359615,
                65114914122427,
                65119679780792,
                65116440364036,
                65118888777635,
                65118363350315,
                65113281896858,
                65114434211247,
                65113329677709,
                65113657883172,
                65114694867938,
                65118116163833,
                65115438556337,
                65111247542880,
                65111015596135,
                65119608228835,
                65113077460543,
                65119996767330,
                65112045672176,
                65116198884645,
                65112947221245,
                65112808799482,
                65115806260727,
                65118869373240,
                86115355663271,
                62111092552316,
                65115995515308,
                65111396577917,
                65116380533420,
                65116210502596,
                65115513239144,
                65119290244994,
                65113566014924,
                65112321131936,
                65115152160850,
                65118077438846,
                65116258953688,
                65118592958651,
                65117682865393,
                65119606257336,
                65119694247664,
                65115130688807,
                65111949904154,
                65116439917561,
                65116165657268,
                65112946985407,
                65115020690782,
                65113269887921,
                65119499393548,
                65111047668629,
                65114163969247,
                65113794712997,
                65115444848418,
                65116673311647,
                65118549510855,
                65119469345896,
                65116608836501,
                65118365801456,
                65118217346418,
                65116524307765,
                65114862072213,
                65114928493766,
                65112902395019,
                65119559432664,
                65119571617420,
                65117699825332,
                65117098700663,
                65113530558543,
                65119803713819,
                65115014722377,
                65119653845839,
                65117101159402,
                65115139131131,
                65116302467069,
                65115634611738,
                65115663292847,
                65118544130071,
                65117109412224,
                65112145030636,
                65119273972149,
                65119320504963,
                62113960088140,
                62115162788227,
                968112799181735,
                62112055682469,
                886118674038029,
                62113809765651,
                65114380316716,
                1117384422228,
                62118042639397,
                49118662156346,
                81112908354754,
                33119662424708,
                63114398269910,
                65118597931475,
                64114376007286,
                1117324527181,
                65113056285714
            ])
            ->get();

        $updated = [];
        $failed = [];

        foreach ($bookings as $booking) {
            $zhColumn = DB::table('sys_ZHBookingType')
                ->where('ZuzuHotelChannel', $booking->Default)
                ->where('NatureOfDistribution', $booking->PropertyType)
                ->where('TravellerPaymentType', (in_array($booking->OTATravellerPaymentToChannel, [3, 4]) ? 2 : 1))
                ->where('ZHResponsibleForInvoicing', $booking->OTAChannelInvoiceHandling)
                ->first();

            if ($zhColumn) {
                $r = BookingModel::where('ID', $booking->ID)->update([
                    'ZHBookingType' => $zhColumn->Id
                ]);

                // Payment handling.
                $invoiceData = $booking->IsBarBooking == 1 ?
                    BookingInvoiceBarData::getBookingInvoiceInformation($booking->ID) :
                    BookingInvoiceData::getBookingInvoiceInformation($booking->ID);

                PaymentData::updateBookingPaymentTables(
                    $booking->ID,
                    $invoiceData
                );
            } else {
                $r = false;
            }

            if ($r) {
                $updated[] = [
                    'ID' => $booking->ID,
                    'R' => $booking->ReservationNumber,
                    'BT' => $zhColumn,
                    'P' => [
                        $booking->Default,
                        $booking->PropertyType,
                        $booking->OTATravellerPaymentToChannel,
                        $booking->OTAChannelInvoiceHandling
                    ]
                ];
            } else {
                $updated[] = [
                    'ID' => $booking->ID,
                    'BT' => $zhColumn,
                    'P' => [
                        $booking->Default,
                        $booking->PropertyType,
                        $booking->OTATravellerPaymentToChannel,
                        $booking->OTAChannelInvoiceHandling
                    ]
                ];
            }
        }

        dd($updated, $failed);
    }

    /**
     * @param Request $request
     */
    public function getUpdateHotelRoomAvailabilityWithTaxes(Request $request)
    {
        set_time_limit(0);
        ini_set('max_execution_time', 3000);
        ini_set('memory_limit', '-1');

        $from = $request->get('from', date('Y-m-d'));
        $to = $request->get('to', date('Y-m-d', strtotime('+2 year', strtotime($from))));

        $hotelObj = $this->roomTypeRepo->getAvailabilityData($from, $to);

        $hotelData = $hotelObj['hotelData'];
        $hotelWithDates = $hotelObj['hotelWithDates'];

        foreach ($hotelWithDates as $key => $value) {

            $rateTaxCalculated = $hotelData[$value->HotelId]['Tax'];
            $rateBaseCalculated = $rateTaxCalculated ? $value->RateCalculated * ((100 - $rateTaxCalculated) / 100) : $value->RateCalculated;

            DB::table('trn_HotelRoomAvailabilityAndRates')
                ->where('HotelId', $value->HotelId)
                ->where('Date', $value->Date)
                ->where('RateCalculated', $value->RateCalculated)
                ->update(['RateTaxCalculated' => $rateTaxCalculated, 'RateBaseCalculated' => $rateBaseCalculated]);

            unset($rateTaxCalculated);
            unset($rateBaseCalculated);

        }

        dd('DONE ' . $from . ' to ' . $to);
    }

    public function getUpdateRoomTypeRateWithTaxes()
    {
        set_time_limit(0);
        ini_set('max_execution_time', 3000);
        ini_set('memory_limit', '-1');

        // Update room type data
        $hotelObj = $this->roomTypeRepo->getRoomTypeRateAvailabilityData();
        $hotelData = $hotelObj['hotelData'];
        $hotelWithRates = $hotelObj['hotelWithRates'];

        foreach ($hotelWithRates as $key => $value) {

            $hotelTax = isset($hotelData[$value->HotelID]) ? $hotelData[$value->HotelID]['Tax'] : null;
            $monRateTaxBase = $hotelTax ? $value->MonRate * 100 / (100 + $hotelTax) : $value->MonRate;
            $tueRateTaxBase = $hotelTax ? $value->TueRate * 100 / (100 + $hotelTax) : $value->TueRate;
            $wedRateTaxBase = $hotelTax ? $value->WedRate * 100 / (100 + $hotelTax) : $value->WedRate;
            $thuRateTaxBase = $hotelTax ? $value->ThuRate * 100 / (100 + $hotelTax) : $value->ThuRate;
            $friRateTaxBase = $hotelTax ? $value->FriRate * 100 / (100 + $hotelTax) : $value->FriRate;
            $satRateTaxBase = $hotelTax ? $value->SatRate * 100 / (100 + $hotelTax) : $value->SatRate;
            $sunRateTaxBase = $hotelTax ? $value->SunRate * 100 / (100 + $hotelTax) : $value->SunRate;

            $updateArray = [
                'MonRateTax' => $hotelTax,
                'MonRateBase' => $monRateTaxBase,
                'TueRateTax' => $hotelTax,
                'TueRateBase' => $tueRateTaxBase,
                'WedRateTax' => $hotelTax,
                'WedRateBase' => $wedRateTaxBase,
                'ThuRateTax' => $hotelTax,
                'ThuRateBase' => $thuRateTaxBase,
                'FriRateTax' => $hotelTax,
                'FriRateBase' => $friRateTaxBase,
                'SatRateTax' => $hotelTax,
                'SatRateBase' => $satRateTaxBase,
                'SunRateTax' => $hotelTax,
                'SunRateBase' => $sunRateTaxBase,
            ];

            DB::table('trn_RoomTypeRate')
                ->where('HotelId', $value->HotelID)
                ->where('RoomTypeID', $value->RoomTypeID)
                ->update($updateArray);

            unset($hotelTax);
            unset($monRateTaxBase);
            unset($tueRateTaxBase);
            unset($wedRateTaxBase);
            unset($thuRateTaxBase);
            unset($friRateTaxBase);
            unset($satRateTaxBase);
            unset($sunRateTaxBase);
        }

        // Update room type linkage data
        $hotelLinkageObj = $this->roomTypeRepo->getRoomTypeRateLinkageAvailabilityData();
        $hotelLinkageData = $hotelLinkageObj['hotelData'];
        $hotelLinkageWithRates = $hotelLinkageObj['hotelWithRates'];

        foreach ($hotelLinkageWithRates as $key => $value) {

            $hotelTax = isset($hotelLinkageData[$value->HotelID]) ? $hotelLinkageData[$value->HotelID]['Tax'] : null;
            $monRateTaxBase = $hotelTax ? $value->MonRate * 100 / (100 + $hotelTax) : $value->MonRate;
            $tueRateTaxBase = $hotelTax ? $value->TueRate * 100 / (100 + $hotelTax) : $value->TueRate;
            $wedRateTaxBase = $hotelTax ? $value->WedRate * 100 / (100 + $hotelTax) : $value->WedRate;
            $thuRateTaxBase = $hotelTax ? $value->ThuRate * 100 / (100 + $hotelTax) : $value->ThuRate;
            $friRateTaxBase = $hotelTax ? $value->FriRate * 100 / (100 + $hotelTax) : $value->FriRate;
            $satRateTaxBase = $hotelTax ? $value->SatRate * 100 / (100 + $hotelTax) : $value->SatRate;
            $sunRateTaxBase = $hotelTax ? $value->SunRate * 100 / (100 + $hotelTax) : $value->SunRate;

            $updateArray = [
                'MonRateTax' => $hotelTax,
                'MonRateBase' => $monRateTaxBase,
                'TueRateTax' => $hotelTax,
                'TueRateBase' => $tueRateTaxBase,
                'WedRateTax' => $hotelTax,
                'WedRateBase' => $wedRateTaxBase,
                'ThuRateTax' => $hotelTax,
                'ThuRateBase' => $thuRateTaxBase,
                'FriRateTax' => $hotelTax,
                'FriRateBase' => $friRateTaxBase,
                'SatRateTax' => $hotelTax,
                'SatRateBase' => $satRateTaxBase,
                'SunRateTax' => $hotelTax,
                'SunRateBase' => $sunRateTaxBase,
            ];

            DB::table('trn_RoomTypeRateLinkage')
                ->where('HotelId', $value->HotelID)
                ->where('RoomTypeID', $value->RoomTypeID)
                ->update($updateArray);

            unset($hotelTax);
            unset($monRateTaxBase);
            unset($tueRateTaxBase);
            unset($wedRateTaxBase);
            unset($thuRateTaxBase);
            unset($friRateTaxBase);
            unset($satRateTaxBase);
            unset($sunRateTaxBase);
        }

        dd('DONE');
    }

    public function getTestTls1_2()
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://pmsupdate.staah.net/common-cgi/Services.pl');
        curl_setopt($ch, CURLOPT_POSTFIELDS, ['test' => 'test data']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        //TLS v1.2
        curl_setopt($ch, CURLOPT_SSLVERSION, 6);

        $result = curl_exec($ch);
        curl_close($ch);

        dd($result);
    }

    public function getCancellationPenaltyUpdate()
    {
        $res = DB::raw('UPDATE set_CancellationPenalties
                        JOIN set_CancellationPolicy ON set_CancellationPolicy.Id = set_CancellationPenalties.CancellationPolicyID
                        SET set_CancellationPenalties.CancellationPenalty = CASE set_CancellationPolicy.CancellationPenalty WHEN
                        NULL THEN 0
                        ELSE set_CancellationPolicy.CancellationPenalty END');
        dd($res);
    }

    public function getUpdateTestBookingsAwaitedTotal()
    {
        dd('done');
        $payments = BookingPaymentModel::select(
            'trn_BookingCompletedPayments.Id AS id'
        )
            ->join(
                'trn_Booking',
                'trn_Booking.ID',
                '=',
                'trn_BookingPayments.BookingID'
            )->join(
                'trn_BookingCompletedPayments',
                'trn_BookingCompletedPayments.Id',
                '=',
                'trn_BookingPayments.BookingCompletedPaymentId'
            )
            ->whereIn('trn_Booking.BookingCategoryId', [
                BookingModel::$testBookingZuzuFunded['test_zh_funded'],
                BookingModel::$testBookingZuzuFunded['test_hotel_funded']
            ])
            ->where('trn_BookingCompletedPayments.PaidStatus', INACTIVE_STATUS)
            ->groupBy('trn_BookingPayments.BookingID')
            ->lists('id');

        $updated = [];
        $failedUpdated = [];

        foreach ($payments as $paymentId) {
            $response = PaymentData::updateCompletedBookingTotal($paymentId, 1);

            if ($response) {
                $updated[] = $paymentId;
            } else {
                $failedUpdated[] = $paymentId;
            }
        }

        dd($updated, $failedUpdated);
    }

    public function getUpdateManaulBookingTaxComponent()
    {
        $updatedB = [];
        $notUpdated = [];

        try {
            dd();
            $bookings = BookingModel::whereIn('ReservationNumber', [request()->get("no")])->get();

            foreach ($bookings as $booking) {
                DB::beginTransaction();

                $enableTax = ZuzuLibs::featureFlagging('tax', $booking->HotelID);
                $taxCollection = ZuzuLibs::getApplicableTaxCollectionForHotel($booking->HotelID);

                $checkedInDate = Carbon::parse($booking->BookingCheckinDate);
                $checkedOutDate = Carbon::parse($booking->BookingCheckoutDate);
                $dateDiff = $checkedOutDate->diffInDays($checkedInDate);

                $totalBaseAmount = 0;

                $rawRateDetails = HotelRatesRawModel::where('HotelId', $booking->HotelID)
                    ->where('RoomTypeId', $booking->HotelRoomTypeID)
                    ->where('RatePlanId', $booking->HotelRatePlanId)
                    ->whereRaw(
                        'Date BETWEEN "' . $checkedInDate->toDateString() . '" AND "' . $checkedOutDate->toDateString() . '"'
                    )
                    ->get();

                $totalAmountForThePeriod = HotelRatesRawModel::selectRaw("SUM(RateCalculated) AS Total, COUNT(*) AS Count")
                    ->where('HotelId', $booking->HotelID)
                    ->where('RoomTypeId', $booking->HotelRoomTypeID)
                    ->where('RatePlanId', $booking->HotelRatePlanId)
                    ->where('Date', '>=', $checkedInDate->toDateString())
                    ->where('Date', '<', $checkedOutDate->toDateString())
                    ->get();

                $recordCount = $totalAmountForThePeriod[0]->Count;
                $totalAmountForThePeriod = $totalAmountForThePeriod[0]->Total;
                $updated = false;
                $stayDates = BookingStayDatesModel::where('BookingID', $booking->ID)->get();;

                do {
                    if ($dateDiff != $recordCount) {
                        $notUpdated[] = $booking->ID;
                    } else {
                        $pickedRaw = $rawRateDetails->where('Date', $checkedInDate->toDateString())->first();
                        $totalAmountForThePeriodNum = $totalAmountForThePeriod == 0 ? 1 : $totalAmountForThePeriod;

                        $pickedStayDate = $stayDates->where('StayDate', $checkedInDate->toDateString())->first();
                        $rateCal = $pickedStayDate->RoomTypeRate * ($pickedStayDate->RoomTypeRateTax + 100) / 100;
                        $tempRateCalculated = ((($rateCal / $totalAmountForThePeriodNum) *
                                $booking->HotelTotalAmount) / $booking->BookingRoomsCount);
                        $getTaxPercentage = ZuzuLibs::getTaxPercentage($taxCollection, $tempRateCalculated);

                        // If tax not enabled for the hotel, get tax % from hotel config.
                        if (!$enableTax) {
                            $getTaxPercentage['tax'] = $booking->HotelTaxRate;
                            $getTaxPercentage['taxesCollection'] = [];
                        }

                        $rateWithoutTax = ($tempRateCalculated * 100 / (100 + $getTaxPercentage['tax']));
                        $rateTaxCalculated = $getTaxPercentage['tax'];
                        $rateCalculatedCollection = $getTaxPercentage['taxesCollection'];

                        $roomTypeAvailability = is_null($pickedRaw) ? null : $pickedRaw->ActualAvailability;
                        $totalBaseAmount += $rateWithoutTax * $booking->BookingRoomsCount;

                        BookingStayDatesModel::where('BookingID', $booking->ID)
                            ->where("StayDate", $checkedInDate->toDateString())
                            ->delete();

                        BookingStayDatesModel::insert([
                            'BookingID' => $booking->ID,
                            'StayDate' => $checkedInDate->toDateString(),
                            'RoomTypeRate' => $rateWithoutTax,
                            'RoomTypeRateTax' => $enableTax ? $rateTaxCalculated : null,
                            'RoomTypeRateTaxCollection' => serialize($rateCalculatedCollection),
                            'RatePlanRoomTypeRate' => $rateWithoutTax,
                            'RoomTypeAvailability' => $roomTypeAvailability
                        ]);

                        $updated = true;
                    }

                    $checkedInDate = $checkedInDate->addDay();
                } while ($checkedInDate->toDateString() < $checkedOutDate->toDateString());

                if ($updated) {
                    $hotelTaxAmount = $booking->HotelTotalAmount - $totalBaseAmount;

                    BookingModel::where('ID', $booking->ID)->update([
                        'HotelTaxAmount' => $hotelTaxAmount,
                        'HotelTaxRate' => ($hotelTaxAmount / $totalBaseAmount) * 100
                    ]);

                    $data = BookingInvoiceBarData::getBookingInvoiceInformation($booking->ID);
                    PaymentData::updateBookingPaymentTables($booking->ID, $data);
                    $updatedB[] = $booking->ID;
                }

                DB::commit();
            }

            dd($updatedB, $notUpdated);
        } catch (Exception $e) {
            DB::rollback();
            dd($e, $updatedB, $notUpdated);
        }
    }

    /**
     * update channel for default values.
     */
    public function getUpdateHotelChannels()
    {
        ini_set('max_execution_time', 300);

        HotelOTAChannelModel::whereIn("TravellerPaymentToChannel", [1, 2])
            ->update([
                "CancellationRule" => 1
            ]);

        HotelOTAChannelModel::whereIn("TravellerPaymentToChannel", [3, 4])
            ->update([
                "CancellationRule" => 2
            ]);

        HotelOTAChannelModel::query()->update([
            "PerformanceDueOnRetained" => 1
        ]);

        HotelOTAChannelModel::query()->update([
            "CommissionDueOnRetained" => 1
        ]);

        $hotels = HotelModel::select("TimeZone")->groupBy("TimeZone")->get();

        foreach ($hotels as $hotel) {
            $actualTimezone = $hotel->TimeZone;
            $date = Carbon::now()->startOfDay();
            try {
                $hotel->TimeZone = str_replace("+", "-", $hotel->TimeZone);
                $date = $date->tz($hotel->TimeZone);
            } catch (Exception $e) {
            }

            HotelModel::where("TimeZone", $actualTimezone)
                ->update(['CancellationCutOffTime' => $date->toTimeString()]);
        }

        //set default values to settings channels.
        $expediaIds = OTAChannelModel::select("Id")
            ->where("Name", "ExpediaPrepay")
            ->orWhere("Name", "ExpediaPostpay")
            ->lists("Id")
            ->toArray();

        $a = ChannelCountryOtaModel::whereIn("SetOtaChannelId", $expediaIds)
            ->update([
                "CutOffTimeOption" => 4
            ]);

        $b = HotelOTAChannelModel::whereIn("OTAChannelId", $expediaIds)->update([
            "CutOffTimeOption" => 4
        ]);

        dd("done", $a, $b);
    }

    /**
     * Update Property Manager Types
     */
    public function getUpdatePropertyManagerTypes()
    {
        try {
            DB::beginTransaction();
            set_time_limit(0);

            HotelUserModel::select("trn_HotelUser.UserId", "trn_HotelUser.HotelID", "set_RoleUsers.user_id")
                ->join('set_RoleUsers', 'set_RoleUsers.user_id', '=', 'trn_HotelUser.UserID')
                ->whereIn('set_RoleUsers.role_id', config('customconfig.property_manager_roles'))
                ->update(['trn_HotelUser.PropertyManagerType' => 0]);

            HotelModel::select("Id")->chunk(50, function ($hotels) {
                foreach ($hotels as $hotel) {
                    $user = HotelUserModel::select("UserId", "HotelID")
                        ->join('set_RoleUsers', 'set_RoleUsers.user_id', '=', 'trn_HotelUser.UserID')
                        ->where('trn_HotelUser.HotelID', $hotel->Id)
                        ->where('trn_HotelUser.PropertyManagerType', 0)
                        ->first();
                    if ($user) {
                        HotelUserModel::where('trn_HotelUser.HotelID', $hotel->Id)
                            ->where('trn_HotelUser.UserId', $user->UserId)
                            ->update(['trn_HotelUser.PropertyManagerType' => 1]);
                    }
                }
            });
            DB::commit();
            dd("done");
        } catch (Exception $e) {
            DB::rollback();
            dd($e->getMessage());
        }
    }

    /**
     * Update hotel config - performance fee options.
     */
    public function getUpdateBusinessModelHotelConfig()
    {
        $expediaPrepay = 1;
        $repo = App::make('Zuzu\Repositories\Hotel\HotelRepository');

        $hotelsToUpdate = HotelModel::select(
            'trn_Hotel.Id',
            'trn_OTAHotelChannel.PerformanceFee',
            'trn_OTAHotelChannel.PerformanceFeeOption',
            'trn_OTAHotelChannel.PerformanceFeeApplicableOn'
        )->leftJoin(
            'trn_ZhPayments',
            'trn_ZhPayments.HotelId',
            '=',
            'trn_Hotel.Id'
        )->join(
            'trn_OTAHotelChannel',
            'trn_OTAHotelChannel.HotelId',
            '=',
            'trn_Hotel.Id'
        )
            ->whereNull('trn_ZhPayments.HotelId')
            ->whereIn('trn_Hotel.Id', [])
            ->where('trn_OTAHotelChannel.OTAChannelId', $expediaPrepay)
            ->get();

        $updated = [];
        $notUpdated = [];
        $now = Carbon::now()->subDay();
        $loggedUser = Sentinel::getUser();

        foreach ($hotelsToUpdate as $hotel) {
            $data = [
                'engagement_book_date_start_date' => null,
                'engagement_book_date_end_date' => null,
                'performance_fee' => true,
                'fixed_fee' => false,
                'performance_payment_level' => 0,
                'performance_fee_from' => [$now->format(DATE_MONTH_YEAR_SLASH_FORMAT)],
                'performance_fee_option' => [$hotel->PerformanceFeeOption],
                'performance_fee_applicable_id' => [$hotel->PerformanceFeeApplicableOn],
                'performance_fee_amount' => [$hotel->PerformanceFee]
            ];

            $response = $repo->addBusinessModelDetails($data, $hotel->Id, ($loggedUser ? $loggedUser->id : 1));

            if ($response) {
                $updated[] = $hotel->Id;
            } else {
                $notUpdated[] = $hotel->Id;
            }
        }

        dd($updated, $notUpdated);
    }

    /**
     * Update country names
     */
    public function getCountryDataAndSave()
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'http://country.io/names.json');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSLVERSION, CURL_SSL_VERSION_TLSv1_2);
        $countries = curl_exec($ch);
        curl_close($ch);

        $countries = json_decode($countries, true);
        DB::beginTransaction();
        try {
            foreach ($countries as $code => $country) {
                DB::table('set_CountryContent')
                    ->where('Code', $code)
                    ->where('LanguageId', 1)
                    ->update(['Country' => $country]);
            }
            DB::commit();
            dd("Updated countries successfully");
        } catch (Exception $e) {
            DB::rollback();
            dd($e);
        }
    }

    private function getChannelMappedRoomTypes($channelId, $propertyId, $channelRoomTypes)
    {
        $types = HotelChannelModel::select('*', 'trn_ChannelRoomTypeMap.Id as channel_room_type_map_id')
            ->join('trn_ChannelRoomTypeMap', 'trn_ChannelRoomTypeMap.HotelChannelId', '=', 'trn_HotelChannel.Id')
            ->join('trn_HotelRoomType', function ($join) {
                $join->on('trn_HotelRoomType.RoomTypeID', '=', 'trn_ChannelRoomTypeMap.RoomType')
                    ->on('trn_HotelRoomType.HotelID', '=', 'trn_HotelChannel.HotelId');
            })->join(
                'set_PredefinedHotelRoomTypeContent',
                'set_PredefinedHotelRoomTypeContent.HotelRoomTypeID',
                '=',
                'trn_HotelRoomType.RoomTypeID'
            )->join('trn_RoomTypeRate', function ($join) {
                $join->on('trn_RoomTypeRate.RoomTypeID', '=', 'trn_ChannelRoomTypeMap.RoomType')
                    ->on('trn_RoomTypeRate.HotelID', '=', 'trn_HotelChannel.HotelId');
            })->where('PropertyId', $propertyId)
            ->where('ChannelId', $channelId);

        if (!empty($channelRoomTypes)) {
            $types = $types->whereIn('ChannelRoomType', $channelRoomTypes);
        }

        $types = $types->groupBy('HotelRoomTypeID');

        return $types->get();
    }

    private function getMappedRatePlan($channelRoomTypeMapId, $ratePlan)
    {
        $zuzuRatePlan = DB::table('trn_ChanelRoomTypeMapRatePlan')
            ->where('ChanelRoomTypeMapId', $channelRoomTypeMapId)
            ->where('RatePlan', $ratePlan)
            ->first();

        return $zuzuRatePlan ? $zuzuRatePlan->ZuzuRatePlan : null;
    }

    public function getUpdateTravellerPaidAmount()
    {
        $bookingId = [
            198874
        ];

        dd('--');

        $updated = [];
        $failed = [];

        foreach ($bookingId as $b) {
            // Get original booking from updates table
            $uB = BookingUpdatesModel::where('ID', $b)->where('AlterationStep', 1)->first();
            $cB = ChannelBookingModel::where('BookingId', $b)->first();

            if ($uB && $cB) {
                // Get last modification XML request from channel xsml table
                $cUX = ChannelBookingXMLModel::where('Xml', 'LIKE', '%<booking_id>' . $cB->ChannelBookingId . '</booking_id>%')
                    ->where('Xml', 'LIKE', '%<booking_status>modify</booking_status>%')
                    ->orderBy('CreatedDate', 'DESC')
                    ->first();

                if ($cUX) {
                    // Format XML to JSON
                    $x = $this->getXml($cUX->Xml);

                    // If room count is more than 1, then this need to be checked manually.
                    if (count($x['room']) == 1) {
                        $room = reset($x['room']);

                        $mappedRooms = $this->getChannelMappedRoomTypes(
                            ChannelModel::$staahId,
                            $x['hotel_id'],
                            array_unique(array_pluck($x['room'], 'room_id'))
                        );

                        $roomType = $mappedRooms->where('ChannelRoomType', $room['room_id'])->first();
                        $zuzuRatePlan = $this->getMappedRatePlan($roomType->channel_room_type_map_id, $room['rate_plan']);

                        $checkArrivalDate1 = Carbon::parse($room['arrival_date']);
                        $checkArrivalDate2 = Carbon::parse($uB->BookingCheckinDate);

                        $checkDepartureDate1 = Carbon::parse($room['departure_date']);
                        $checkDepartureDate2 = Carbon::parse($uB->BookingCheckoutDate);

                        $hotelRatePlan = HotelRatePlanModel::where('HotelId', $uB->HotelID)
                            ->where(
                                !is_null($zuzuRatePlan) ? 'Id' : 'Default',
                                !is_null($zuzuRatePlan) ? $zuzuRatePlan : IS_DEFAULT
                            )
                            ->first();

                        // Check modified data which effects the total booking amount.
                        if (($checkArrivalDate1->ne($checkArrivalDate2)) ||
                            ($checkDepartureDate1->ne($checkDepartureDate2)) ||
                            ($room['numberofguests'] != $uB->BookingGuestCount) ||
                            ($room['number_of_rooms'] != $uB->BookingRoomsCount) ||
                            ($roomType->RoomType != $uB->HotelRoomTypeID) ||
                            ($hotelRatePlan->Id != $uB->HotelRatePlanId)
                        ) {
                            // Get calculated new TravellerOrigianllyOwed
                            $bd = BookingInvoiceBarData::getBookingInvoiceInformation($b);

                            // Get TravellerOrigianllyOwed FROM db
                            $bdE = DB::table('trn_BookingPaymentDetails')->where('BookingId', $b)->first();

                            $bN = number_format(
                                ($bd['net_amount_including_tax'] - $bd['traveller_paid_time_of_booking']),
                                2,
                                '.',
                                ''
                            );

                            // iF db value mismatched the calc amount, update it.
                            If ($bN != $bdE->TravellerOriginallyOwned) {
                                $res = DB::table('trn_BookingPaymentDetails')->where('BookingId', $b)->update([
                                    'TravellerOriginallyOwned' => $bN
                                ]);

                                $updated[] = [
                                    'r' => $res,
                                    'b' => $b
                                ];
                            } else {
                                $failed[] = [
                                    'E' => $b
                                ];
                            }
                        } else {
                            $failed[] = [
                                'N' => $b
                            ];
                        }
                    } else {
                        $failed[] = [
                            'M' => $b
                        ];
                    }
                } else {
                    $failed[] = [
                        'XML' => $b
                    ];
                }
            } else {
                $failed[] = [
                    'ID' => $b
                ];
            }
        }

        dd($updated, $failed);
    }

    private function replaceBookingSource($response)
    {
        try {
            $response['reservation']['actual_company'] = $response['reservation']['company'];
            $map = $this->channelRepo->getMappedOtaSource($response['reservation']['company'], ChannelModel::$staahId);

            if ($map) {
                $response['reservation']['company'] = $map->Name;
            }

            return $response;
        } catch (Exception $e) {
            AppLogger::recordAppErrors($e, Sentinel::getUser(), request()->getUri());
            return $response;
        }
    }

    private function getXml($xml)
    {
        $xml2 = simplexml_load_string($xml);

        $json = json_encode($xml2);
        $staahBooking = json_decode($json, true);

        if (!is_array(array_values($staahBooking['reservation']['room'])[0])) {
            $staahBooking['reservation']['room'] = [$staahBooking['reservation']['room']];
        }

        $bookedRoom = [];

        foreach (simplexml_load_string($xml)->reservation->room as $room) {
            $formatted = [];
            foreach ($room->price as $price) {
                if (array_key_exists($price['rate_id']->__toString(), $formatted)) {
                    $formatted[$price['rate_id']->__toString()][] = $price['date']->__toString();
                } else {
                    $formatted[$price['rate_id']->__toString()] = [$price['date']->__toString()];
                }
            }

            $c = 0;
            foreach ($room->price as $key => $price) {
                $arrKey = $room->arrival_date . '_' . $room->departure_date . "_" . $room->room_id . "_" .
                    $price['rate_id'];

                if (array_key_exists($arrKey, $bookedRoom)) {
                    $bookedRoom[$arrKey]['totalprice'] += $price->__toString();
                    if (isset($bookedRoom[$arrKey]['stay_date_price'][$price['date']->__toString()])) {
                        $bookedRoom[$arrKey]['stay_date_price'][$price['date']->__toString()] +=
                            $price->__toString();
                    } else {
                        $bookedRoom[$arrKey]['stay_date_price'][$price['date']->__toString()] =
                            $price->__toString();
                    }
                } else {
                    $bookedRoom[$arrKey] = [];
                    foreach ($room as $k => $r) {
                        $bookedRoom[$arrKey][$k] = $r->__toString();
                    }

                    $bookedRoom[$arrKey]['stay_date_price'][$price['date']->__toString()] = $price->__toString();
                    $rateId = $formatted[$price['rate_id']->__toString()];
                    $bookedRoom[$arrKey]['rate_plan'] = $price['rate_id']->__toString();
                    $bookedRoom[$arrKey]['totalprice'] = $price->__toString();
                    $bookedRoom[$arrKey]['arrival_date'] = $rateId[0];
                    $bookedRoom[$arrKey]['departure_date'] = Carbon::parse(end($rateId))->addDay()->toDateString();
                    $bookedRoom[$arrKey]['numberofguests'] = $price['numberofadult']->__toString();
                    $bookedRoom[$arrKey]['numberofchild'] = $price['numberofchild']->__toString();
                }

                if (!array_key_exists('number_of_rooms', $bookedRoom[$arrKey])) {
                    $bookedRoom[$arrKey]['number_of_rooms'] = 1;
                } elseif ($c == 0) {
                    $bookedRoom[$arrKey]['number_of_rooms'] += 1;
                }

                $c++;
            }
        }

        $staahBooking['reservation']['room'] = $bookedRoom;
        $staahBooking = $this->replaceBookingSource($staahBooking);

        foreach ($staahBooking['reservation']['customer'] as $key => $data) {
            if (is_array($data) && empty($data)) {
                if ($key == "countrycode") {
                    $staahBooking['reservation']['customer'][$key] = DEFAULT_COUNTRY;
                } elseif ($key == "first_name") {
                    $staahBooking['reservation']['customer'][$key] =
                        !empty($data['last_name']) ? $data['last_name'] : '';
                } elseif ($key == "last_name") {
                    $staahBooking['reservation']['customer'][$key] =
                        !empty($data['first_name']) ? $data['first_name'] :
                            (empty($staahBooking['reservation']['customer']['last_name']) &&
                            empty($staahBooking['reservation']['customer']['first_name']) ? 'ZUZU' : '');
                } elseif ($key == "address") {
                    $staahBooking['reservation']['customer'][$key] = 'N/A';
                } elseif ($key == "city") {
                    $staahBooking['reservation']['customer'][$key] = 'N/A';
                } elseif ($key == "email") {
                    //
                } elseif ($key == "telephone") {
                    $staahBooking['reservation']['customer'][$key] = '+00000000000';
                } elseif ($key == "zip") {
                    $staahBooking['reservation']['customer'][$key] = '';
                } elseif ($key == "remarks") {
                    $staahBooking['reservation']['customer'][$key] = '';
                }
            }
        }

        return $staahBooking['reservation'];
    }

    public function getUpdateHostelWorldBookings()
    {

        $select = 'SELECT BookingId FROM trn_ChannelBooking WHERE' .
            ' OTAChannelId = 9 AND CreatedDate > "2018-01-01" AND' .
            ' ChannelPaymentMapOptions LIKE "%5%" AND ' .
            ' BookingId NOT IN (' .
            ' SELECT trn_BookingPayments.BookingID FROM' .
            ' trn_BookingPayments JOIN trn_BookingCompletedPayments ON' .
            ' trn_BookingPayments.BookingCompletedPaymentId = trn_BookingCompletedPayments.Id WHERE' .
            ' trn_BookingCompletedPayments.PaidStatus = 1 AND' .
            ' trn_BookingPayments.BookingID IN' .
            ' (SELECT BookingId FROM trn_ChannelBooking WHERE OTAChannelId = 9 AND CreatedDate > "2018-01-01"))';

        $bookingIds = DB::select($select);
//        dd($bookingIds);

        foreach ($bookingIds as $bb) {
            $b = $bb->BookingId;

            try {
                // Get original booking from updates table
                $cB = ChannelBookingModel::where('BookingId', $b)->first();
                if ($cB) {
                    $clonedBooking = BookingModel::where('ID', $b)->first();
                    // Get last modification XML request from channel xsml table
                    $cUX = ChannelBookingXMLModel::where('Xml', 'LIKE', '%<booking_id>' . $cB->ChannelBookingId . '</booking_id>%')
                        ->orderBy('CreatedDate', 'DESC')
                        ->first();

                    if ($cUX) {
                        // Format XML to JSON
                        $data = $this->getXml($cUX->Xml);

                        $hotel = $this->bookingRepo->getMappedHotelForGivenChannel($data['hotel_id'], ChannelModel::$staahId);
                        $isBarHotel = $hotel->OnBar == 1;
                        $hotelCurrency = $this->bookingRepo->getHotelCurrencyById($hotel->CurrencyID);
                        $bookingCurrency = $this->bookingRepo->getCurrencyByCode($data['currencycode']);

                        foreach ($data['room'] as $room) {

                            $hotelAmountData = BookingInvoiceBarData::getBookingAmountDetails(
                                $data,
                                $room,
                                ChannelModel::$staahId,
                                $hotel
                            );

                            $hotel->Tax = $hotelAmountData['booking_tax_percentage'];
                            $hotelTotalAmount = $hotelAmountData['booking_total_including_tax'];
                            $commission = $hotelAmountData['booking_markup_amount'];
                            $hotelTaxAmount = $hotelAmountData['booking_tax_amount'];

                            $clientTotalAmount = $hotelAmountData['booking_total_including_tax'] /
                                $hotelCurrency->CurrencyRate * $bookingCurrency->CurrencyRate;
                            $clientTaxAmount = $hotelAmountData['booking_tax_amount'] /
                                $hotelCurrency->CurrencyRate * $bookingCurrency->CurrencyRate;

                            $totalAmountSGD = $hotelAmountData['booking_total_including_tax'] / $hotelCurrency->CurrencyRate;
                            $taxAmountSGD = $hotelAmountData['booking_tax_amount'] / $hotelCurrency->CurrencyRate;
                            $commissionAmountSGD = $hotelAmountData['booking_markup_amount'] / $hotelCurrency->CurrencyRate;

                            $clonedBooking->BookingTotalAmount = round($totalAmountSGD, 2);
                            $clonedBooking->BookingHotelTaxAmount = $taxAmountSGD;
                            $clonedBooking->BookingHotelCommissionAmount = $isBarHotel ? 0 :
                                round($commissionAmountSGD, PRICE_MAX_DECIMAL);

                            $clonedBooking->PriceModelTotalAmount = $hotelAmountData['booking_total_including_tax'] /
                                $bookingCurrency->CurrencyRate;

                            $clonedBooking->PriceModelClientTotalAmount = $hotelAmountData['booking_total_including_tax'];
                            $clonedBooking->PriceModelClientTaxAmount = getTax(
                                $hotelAmountData['booking_total_including_tax'] * 100 / (100 + $hotel->Tax),
                                $hotel->Tax
                            );
                            $clonedBooking->ClientTotalAmount = $clientTotalAmount;

                            $clonedBooking->HotelTotalAmount = round($hotelTotalAmount);
                            $clonedBooking->HotelTaxAmount = $hotelTaxAmount;
                            $clonedBooking->HotelCommissionAmount = $isBarHotel ? 0 : $commission;

                            $clonedBooking->HotelTaxRateCollection = $hotelAmountData['tax_collection'] ?
                                serialize($hotelAmountData['tax_collection']) : null;

                            $clonedBooking->save();

                            $bookings[] = [
                                'booking' => $clonedBooking,
                                'additional_data' => $hotelAmountData,
                                'room_node' => $room
                            ];

                            // Payment handling.
                            $invoiceData = $clonedBooking->IsBarBooking == 1 ?
                                BookingInvoiceBarData::getBookingInvoiceInformation($b) :
                                BookingInvoiceData::getBookingInvoiceInformation($b);

                            PaymentData::updateBookingPaymentTables(
                                $b,
                                $invoiceData,
                                false,
                                null,
                                true
                            );

                        }

                        DB::commit();

                        $savedBookings = [];
                        foreach ($bookings as $bookingArr) {
                            $savedBookings[] = $bookingArr;
                        }

                        $formattedResponse = $data;
                        $formattedResponse["channel_ref_post_fix_starts"] =
                            array_key_exists("existing_bookings", $data) ?
                                $data["existing_bookings"] : 0;
                        $formattedResponse["is_br_booking"] = 0;

                        //add channel booking record/s.
                        $allBookingAmounts = $this->bookingRepo->getAmountsForTotalBooking($data, $isBarHotel, $formattedResponse, $hotel);
                        $this->bookingRepo->updateChannelResponse(
                            $clonedBooking->HotelID,
                            $savedBookings,
                            ChannelModel::$staahId,
                            $formattedResponse,
                            $cUX,
                            $formattedResponse,
                            $allBookingAmounts
                        );
                    }

                    unset($clonedBooking);
                }
                dd("success");

            } catch (Exception $e) {
                dd("error", $e);
            }
        }
    }

    /**
     *
     */
    public function getAddRoomTypeNamesForHotels()
    {
        $hotelRoomTypes = RoomTypeModel::get();

        foreach ($hotelRoomTypes->groupBy('HotelID') as $hotelRoomType) {
            $job = (new AddRoomTypeNameHotelJob($hotelRoomType))->onQueue('room_type_name');
            $this->dispatch($job);
        }
    }

    public function getFindMissedRoomRatesAndAvailabilities(Request $request)
    {
        $hotelId = $request['hotelId'];
        $roomTypeId = $request['roomTypeId'];
        $ratePlanId = $request['ratePlanId'];

        try {
            DB::beginTransaction();

            $hotelRatesAndAvailabilities = HotelRatesRawModel::where('HotelId', $hotelId)
                ->where('RoomTypeId', $roomTypeId)
                ->where('RatePlanId', $ratePlanId)
                ->orderBy('Date', 'asc')
                ->get();

            $availabilityDate = null;
            $priorDateForMissingDataList = [];
            foreach ($hotelRatesAndAvailabilities as $rateAndAvailability) {
                if (!$availabilityDate) {
                    $availabilityDate = Carbon::parse($rateAndAvailability['Date']);
                }
                if ($rateAndAvailability['Date'] != $availabilityDate->toDateString()) {
                    $priorDateForMissingDataList[] = $availabilityDate->toDateString();

                    $availabilityDate->addDay(1);
                    while ($rateAndAvailability['Date'] != $availabilityDate->toDateString()) {
                        $priorDateForMissingDataList[] = $availabilityDate->toDateString();

                        $availabilityDate->addDay(1);
                    }
                    $availabilityDate->addDay(1);
                } else {
                    $availabilityDate->addDay(1);
                }
            }

            $notAddedDates = [];
            $insertArray = [];
            if (!empty($priorDateForMissingDataList)) {
                foreach ($priorDateForMissingDataList as $missingDate) {
                    $preMissingDate = Carbon::parse($missingDate)
                        ->subWeek(1);
                    $postMissingDate = Carbon::parse($missingDate)
                        ->addDay(1);

                    while ($hotelRatesAndAvailabilities->where('Date', $preMissingDate->toDateString())->isEmpty()) {
                        $preMissingDate->subWeek(1);
                    }
                    $preAvailabilityRates = $hotelRatesAndAvailabilities
                        ->where('Date', $preMissingDate->toDateString())
                        ->first();

                    while ($hotelRatesAndAvailabilities->where('Date', $postMissingDate->toDateString())->isEmpty()) {
                        $postMissingDate->addWeek(1);
                    }
                    $postAvailabilityRates = $hotelRatesAndAvailabilities
                        ->where('Date', $postMissingDate->toDateString())
                        ->first();

                    $boolAvailability = $preAvailabilityRates['Availability'] == $postAvailabilityRates['Availability'];
                    $boolTotalBookings = $preAvailabilityRates['TotalBookings'] == $postAvailabilityRates['TotalBookings'];
                    $boolActualAvailability = $preAvailabilityRates['ActualAvailability'] == $postAvailabilityRates['ActualAvailability'];
                    $boolRateCalculated = $preAvailabilityRates['RateCalculated'] == $postAvailabilityRates['RateCalculated'];

                    if ($boolAvailability && $boolTotalBookings && $boolActualAvailability && $boolRateCalculated) {
                        if ($preAvailabilityRates && $postAvailabilityRates) {
                            $newHotelRatesAvailability = $preAvailabilityRates->toArray();
                            $newHotelRatesAvailability['Date'] = $missingDate;

                            $insertArray[] = $newHotelRatesAvailability;
                        }
                    } else {
                        $notAddedDates[] = $missingDate;
                    }
                }
            }
            $this->hotelRatesRawModel->insert($insertArray);

            DB::commit();
            dd("Operation finished!", "missing dates", $priorDateForMissingDataList, "not added dates", $notAddedDates);

        } catch (Exception $e) {
            DB::rollback();
            dd($e);
        }
    }


    /* update all payment details
    *
     *
     */

    public function getUpdatePaymentDetails()
    {
        try {
            $bookingIds = [
                250680,
                250690,
                250708,
                250720,
                250737,
                250739,
                250742,
                250769,
                250775,
                250776,
                250777,
                250798,
                250814,
                250818,
                250870,
                250880,
                250892,
                250895,
                250901,
                250906,
                250907,
                250910,
                250914,
                250925,
                250926,
                250946,
                250975,
                251008,
                251009,
                251010,
                251011,
                251012,
                251013,
                251059,
                251060,
                251061,
                251062,
                251063,
                251064,
                251065,
                251078,
                251112,
                251119,
                251128,
                251130,
                251133,
                251139,
                251158,
                251159,
                251186,
                251208,
                251224,
                251227,
                251231,
                251242,
                251253,
                251256,
                251270,
                251274,
                251278,
                251288,
                251290,
                251293,
                251296,
                251319,
                251336,
                251337,
                251338,
                251339,
                251353,
                251354,
                251357,
                251365,
                251366,
                251380,
                251381,
                251386,
                251414,
                251417,
                251442,
                251479,
                251486,
                251523,
                251531,
                251554,
                251563,
                251564,
                251573,
                251578,
                251586,
                251595,
                251608,
                251616,
                251642,
                251655,
                251656,
                251686,
                251733,
                251750,
                251770,
                251784,
                251788,
                251789,
                251840,
                251867,
                251882,
                251895,
                251906,
                251923,
                251934,
                251942,
                251943,
                251985,
                251986,
                252011,
                252032,
                252043,
                252051,
                252052,
                252073,
                252119,
                252181,
                252237,
                252253,
                252271,
                252286,
                252290,
                252293,
                252301,
                252316,
                252348,
                252355,
                252375,
                252383,
                252386,
                252387,
                252400,
                252406,
                252409,
                252410,
                252474,
                252478,
                252479,
                252483,
                252488,
                252510,
                252528,
                252536,
                252537,
                252547,
                252568,
                252580,
                252590,
                252591,
                252594,
                252623,
                252638,
                252653,
                252654,
                252662,
                252712,
                252713,
                252714,
                252715,
                252737,
                252738,
                252739,
                252764,
                252779,
                252797
            ];
            $bookingIds2 = [
                41322,
                41324,
                41325,
                41323,
                41321,
                41311,
                41310,
                41233,
                41232,
                41231,
                41312,
                41313,
                41314,
                41315,
            ];
            $updated = [];
            $failed = [];

            $xmls = [];

            foreach ($bookingIds2 as $b) {
                // Get original booking from updates table
                $channelBooking = ChannelBookingModel::where('BookingId', $b)->first();

                if ($channelBooking) {
                    // Get last modification XML request from channel xsml table
                    $channelBookingXml = ChannelBookingXMLModel::where('Xml', 'LIKE', '%<booking_id>' .
                        $channelBooking->ChannelBookingId . '</booking_id>%')
                        ->where('Xml', 'LIKE', '%<booking_status>new</booking_status>%')
                        ->orderBy('CreatedDate', 'DESC')
                        ->first();

                    if ($channelBookingXml) {
                        $formattedXMl = $this->getXml($channelBookingXml->Xml);

                        $hotel = $this->bookingRepo
                            ->getMappedHotelForGivenChannel($formattedXMl['hotel_id'], ChannelModel::$staahId);
                        $hotelCurrency = BookingProcess::getHotelCurrency($hotel->CurrencyID);
                        $bookingCurrency = BookingProcess::getCurrency($formattedXMl['currencycode'], 'CurrencyCode');
                        foreach ($formattedXMl['room'] as $room) {

                            dd($formattedXMl);
                            $hotelAmountData = BookingInvoiceBarData::getBookingAmountDetails(
                                $formattedXMl,
                                $room,
                                ChannelModel::$staahId,
                                $hotel
                            );

                            try {
                                DB::beginTransaction();
                                $hotel->Tax = $hotelAmountData['booking_tax_percentage'];
                                $hotelTotalAmount = $hotelAmountData['booking_total_including_tax'];
                                $hotelTaxAmount = $hotelAmountData['booking_tax_amount'];
                                $clientTotalAmount = $hotelAmountData['booking_total_including_tax'] /
                                    $hotelCurrency->CurrencyRate * $bookingCurrency->CurrencyRate;
                                $totalAmountSGD = $hotelAmountData['booking_total_including_tax']
                                    / $hotelCurrency->CurrencyRate;
                                $taxAmountSGD = $hotelAmountData['booking_tax_amount'] / $hotelCurrency->CurrencyRate;


                                $bookingUpdateData = [
                                    'BookingHotelTaxAmount' => $taxAmountSGD,
                                    'HotelTaxAmount' => $hotelTaxAmount,
                                    'ClientTotalAmount' => $clientTotalAmount,
                                    'BookingTotalAmount' => round($totalAmountSGD, 2),
                                    'PriceModelTotalAmount' => $hotelAmountData['booking_total_including_tax'] /
                                        $bookingCurrency->CurrencyRate,
                                    'PriceModelClientTotalAmount' => $hotelAmountData['booking_total_including_tax'],
                                    'PriceModelClientTaxAmount' => ZuzuLibs::getTax(
                                        $hotelAmountData['booking_total_including_tax'] * 100 / (100 + $hotel->Tax), $hotel->Tax),
                                    'HotelTaxRateCollection' => $hotelAmountData['tax_collection'] ?
                                        serialize($hotelAmountData['tax_collection']) : null

                                ];

                                if ($formattedXMl["is_br_booking"]) {
                                    $bookingUpdateData = [
                                        'ClientExtraAdultRateCalculated' => ($room["extraadultrate"]
                                            * $hotelTotalAmount),
                                        'ClientExtraChildRateCalculated' => ($room["extrachildrate"]
                                                * $hotelTotalAmount) / $room["totalprice"]
                                    ];
                                }

                                BookingModel::where('Id', $b)->update([$bookingUpdateData]);

                                $company = $formattedXMl['actual_company'];
                                $paymentType = isset($formattedXMl['paymenttype']) ? $formattedXMl['paymenttype'] : null;
                                $setting = $this->getMapSettings($hotel->ID, ChannelModel::$staahId, $company, $paymentType);
                                $mapOptions = $this->getMapOptions($setting->MapId);

                                ChannelBookingModel::where('BookingId', $b)->update([
                                    'ChannelPaymentMapOptions' => serialize($mapOptions)
                                ]);


                            } catch (Exception $e) {
                                DB::rollBack();
                            }
                        }
                    }
                }
            }
        } catch (Exception $e) {

            dd($e);
        }
    }

    /**
     * Get excel file logs for changing booking inventory
     */
    public function getFixBookingInventoryLogs(Request $request)
    {
        try {
            if (isset($request->file)) {
                return response()->download(storage_path('exports/' . $request->file));
            } else {
                return [
                    'message' => 'filename is required'
                ];
            }
        } catch (Exception $e) {
            return [
                'message' => $e->getMessage()
            ];
        }
    }

    public function getXyZ()
    {
        dd(trans('administration.left_menu.welcome', [], 'messages', 'zh-TW'));
    }

    /**
     * main function for comparision part in PHP language files
     */
    public function getPhplang()
    {
        $englishLang = File::getRequire(base_path() . '/resources/lang/en/administration.php');         //get the whole English language file
        $bahasaLang = File::getRequire(base_path() . '/resources/lang/id/administration.php');          //get the whole Bahasa language file
        $chineseHKLang = File::getRequire(base_path() . '/resources/lang/zh-HK/administration.php');    //get the whole Chinese HK language file
        $chineseTWLang = File::getRequire(base_path() . '/resources/lang/zh-TW/administration.php');    //get the whole Chinese TW language file

        $builtEngLang = $this->buildLangArrays($englishLang);
        $builtBahasaLang = $this->buildLangArrays($bahasaLang);
        $builtChineseHKLang = $this->buildLangArrays($chineseHKLang);
        $builtChineseTWLang = $this->buildLangArrays($chineseTWLang);
        //$langArray = array($builtEngLang, $builtBahasaLang, $builtChineseHKLang, $builtChineseTWLang);

        $orderedBahasaLangArray = $this->orderLangArray($builtEngLang[0], $builtBahasaLang);             //get the order Bahasa Language Array according to the English Key
        $orderedChineseHKLangArray = $this->orderLangArray($builtEngLang[0], $builtChineseHKLang);       //get the order Bahasa Language Array according to the English Key
        $orderedChineseTWLangArray = $this->orderLangArray($builtEngLang[0], $builtChineseTWLang);       //get the order Bahasa Language Array according to the English Key

        $builtEnglishLangArrayKeys = $builtEngLang[0];                                                  //get the English Array Key set
        $builtEnglishLangArrayValues = $builtEngLang[1];                                                //get the English Array Value set
        $orderedBahasaLangArrayKeys = $orderedBahasaLangArray[0];                                       //get the ordered Bahasa Language Array Key set according to the English key
        $orderedBahasaLangArrayValues = $orderedBahasaLangArray[1];                                     //get the ordered Bahasa Language Array Value set
        $orderedChineseHKLangArrayKeys = $orderedChineseHKLangArray[0];                                 //get the ordered ChineseHK Language Array Key set according to the English key
        $orderedChineseHKLangArrayValues = $orderedChineseHKLangArray[1];                               //get the ordered ChineseHK Language Array Value set
        $orderedChineseTWLangArrayKeys = $orderedChineseTWLangArray[0];                                 //get the ordered ChineseTW Language Array Key set according to the English key
        $orderedChineseTWLangArrayValues = $orderedChineseTWLangArray[1];                               //get the ordered ChineseTW Language Array Value set

        //return the all Languages keys and values to the langs blade file
        return view('langs', [
            'builtEnglishLangArrayKeys' => $builtEnglishLangArrayKeys,
            'builtEnglishLangArrayValues' => $builtEnglishLangArrayValues,
            'orderedBahasaLangArrayKeys' => $orderedBahasaLangArrayKeys,
            'orderedBahasaLangArrayValues' => $orderedBahasaLangArrayValues,
            'orderedChineseHKLangArrayKeys' => $orderedChineseHKLangArrayKeys,
            'orderedChineseHKLangArrayValues' => $orderedChineseHKLangArrayValues,
            'orderedChineseTWLangArrayKeys' => $orderedChineseTWLangArrayKeys,
            'orderedChineseTWLangArrayValues' => $orderedChineseTWLangArrayValues
        ]);
    }

    /**
     * main function for JSON comparision part in language files
     */
    public function getJsonlang()
    {

        $jsonEngContent = file_get_contents(base_path() . '/public/locales/en/translation.json');
        $jsonBahasaContent = file_get_contents(base_path() . '/public/locales/id/translation.json');
        $jsonChineseHKContent = file_get_contents(base_path() . '/public/locales/zh-HK/translation.json');
        $jsonChineseTWContent = file_get_contents(base_path() . '/public/locales/zh-TW/translation.json');

        $arrayEng = json_decode($jsonEngContent, true);
        $arrayBahasa = json_decode($jsonBahasaContent, true);
        $arrayChineseHK = json_decode($jsonChineseHKContent, true);
        $arrayChineseTW = json_decode($jsonChineseTWContent, true);

        $builtEngLang = $this->buildLangArrays($arrayEng);
        $builtBahasaLang = $this->buildLangArrays($arrayBahasa);
        $builtChineseHKLang = $this->buildLangArrays($arrayChineseHK);
        $builtChineseTWLang = $this->buildLangArrays($arrayChineseTW);

        $orderedBahasaLangArray = $this->orderLangArray($builtEngLang[0], $builtBahasaLang);             //get the order Bahasa Language Array according to the English Key
        $orderedChineseHKLangArray = $this->orderLangArray($builtEngLang[0], $builtChineseHKLang);       //get the order Bahasa Language Array according to the English Key
        $orderedChineseTWLangArray = $this->orderLangArray($builtEngLang[0], $builtChineseTWLang);       //get the order Bahasa Language Array according to the English Key

        $builtEnglishLangArrayKeys = $builtEngLang[0];                                                  //get the English Array Key set
        $builtEnglishLangArrayValues = $builtEngLang[1];                                                //get the English Array Value set
        $orderedBahasaLangArrayKeys = $orderedBahasaLangArray[0];                                       //get the ordered Bahasa Language Array Key set according to the English key
        $orderedBahasaLangArrayValues = $orderedBahasaLangArray[1];                                     //get the ordered Bahasa Language Array Value set
        $orderedChineseHKLangArrayKeys = $orderedChineseHKLangArray[0];                                 //get the ordered ChineseHK Language Array Key set according to the English key
        $orderedChineseHKLangArrayValues = $orderedChineseHKLangArray[1];                               //get the ordered ChineseHK Language Array Value set
        $orderedChineseTWLangArrayKeys = $orderedChineseTWLangArray[0];                                 //get the ordered ChineseTW Language Array Key set according to the English key
        $orderedChineseTWLangArrayValues = $orderedChineseTWLangArray[1];                               //get the ordered ChineseTW Language Array Value set

        //return the all Languages keys and values to the langs blade file
        return view('langJson', [
            'builtEnglishLangArrayKeys' => $builtEnglishLangArrayKeys,
            'builtEnglishLangArrayValues' => $builtEnglishLangArrayValues,
            'orderedBahasaLangArrayKeys' => $orderedBahasaLangArrayKeys,
            'orderedBahasaLangArrayValues' => $orderedBahasaLangArrayValues,
            'orderedChineseHKLangArrayKeys' => $orderedChineseHKLangArrayKeys,
            'orderedChineseHKLangArrayValues' => $orderedChineseHKLangArrayValues,
            'orderedChineseTWLangArrayKeys' => $orderedChineseTWLangArrayKeys,
            'orderedChineseTWLangArrayValues' => $orderedChineseTWLangArrayValues
        ]);
    }

    /**
     * function to build the language array as Key and Value
     */
    public function buildLangArrays($langArray)
    {
        $keyArray = [];
        $valueArray = [];
        foreach ($langArray as $key => $value) {

            if (is_array($value)) {
                foreach ($value as $xKey => $xValue) {
                    if (is_array($xValue)) {
                        foreach ($xValue as $yKey => $yValue) {
                            if (is_array($yValue)) {
                                foreach ($yValue as $zKey => $zValue) {
                                    if (is_array($zValue)) {
                                        foreach ($zValue as $qKey => $qValue) {
                                            $keyArray[] = sprintf("%s - %s - %s - %s - %s ", $key, $xKey, $yKey, $zKey, $qKey);
                                            $valueArray[] = $qValue;
                                        }
                                    } else {
                                        $keyArray[] = sprintf("%s - %s - %s - %s ", $key, $xKey, $yKey, $zKey);
                                        $valueArray[] = $zValue;
                                    }
                                }
                            } else {
                                $keyArray[] = sprintf("%s - %s - %s ", $key, $xKey, $yKey);
                                $valueArray[] = $yValue;
                            }
                        }
                    } else {
                        $keyArray[] = sprintf("%s - %s ", $key, $xKey);
                        $valueArray[] = $xValue;
                    }
                }
            } else {
                $keyArray[] = $key;
                $valueArray[] = $value;
            }
        }
        $mappedLangArray = array($keyArray, $valueArray);
        return $mappedLangArray;
    }

    /**
     * function to order the Language array according to the English key
     */
    public function orderLangArray($englishKeyArray, $langArray)
    {                                    //function to order the Language array according to the English key
        $orderedKeyArray = [];
        $orderedValueArray = [];

        $langKeyArray = $langArray[0];
        $langValueArray = $langArray[1];

        foreach ($englishKeyArray as $englishKey) {
            $langKeyIndex = array_search($englishKey, $langKeyArray);
            $langValue = $langValueArray[$langKeyIndex];
            $orderedValueArray[] = $langValue;
            $orderedKeyArray[] = $englishKey;
        }

        $orderedLangArray = array($orderedKeyArray, $orderedValueArray);
        return $orderedLangArray;

    }


}